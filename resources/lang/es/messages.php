<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during .
    |
    */

    'welcome' => 'Bienvenido',
    'landing' => "La página de inicio de su aplicación.",
    'delete' => "Está seguro de borrar este elemento?",

    'actions' => "acciones",
    'action' => 'acción',
    'sections' => "secciones",
    'section' => 'sección',
    'subsections' => "subsecciones",
    'subsection' => 'subsección',
    'role' => 'rol',
    'roles' => 'roles',
    'user' => 'usuario',
    'users' => 'usuarios',
    'institution' => 'institución',
    'institutions' => 'instituciones',
    'division' => 'división',
    'divisions' => 'divisiones',
    'room' => 'habitación',
    'rooms' => 'habitaciones',
    'modality' => 'modalidad',
    'modalities' => 'modalidades',
    'equipment' => 'equipo',
    'referring' => 'referente',
    'referrings' => 'referentes',
    'configuration' => 'configuración',
    'configurations' => 'configuraciones',
    'patient-types' => 'tipos de pacientes',
    'patient-type' => 'tipo de paciente',
    'ubication' => 'ubicación',
    'ubications' => 'ubicaciones',
    'source' => 'procedencia',
    'sources' => 'procedencias',
    'procedure' => 'procedimiento',
    'procedures' => 'procedimientos',
    'template' => 'plantilla',
    'templates' => 'plantillas',
    'consumable' => 'consumible',
    'consumables' => 'consumibles',
    'suspendreason' => 'suspender la razon',
    'suspendreasons' => 'razones de suspensi&oacute;n',

    'password' => 'contraseña',
    'lbl-password-confirmation' => 'Confirma Contrase&ntilde;a',
    'spn-reset-password' => 'Cambiar Contrase&ntilde;a',
    'remember' => 'recordar',
    'forgot-password' => '¿Olvidó su Contraseña?',
    'login' => 'iniciar sesión',
    'btn-send' => 'Enviar',
    'lbl-email' => 'Correo Electr&oacute;nico',

    'success-add' => ':name agregad@ con éxito',
    'error-add' => ':name sin agregar',
    'success-edit' => ':name editad@ con éxito',
    'error-edit' => ':name no editad@',
    'success-delete' => ':name eliminad@ con éxito',
    'error-delete' => ':name no eliminad@',

    'show' => 'ver',
    'add' => 'crear',
    'edit' => 'editar',
    'purge' => 'eliminar',

];
