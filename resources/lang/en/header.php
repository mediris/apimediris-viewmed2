<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during
    |
    */

    'home' => 'Home',
    'login' => 'Login',
    'register' => 'Register',
    'welcome' => 'Hello :prefix :user',
    'logout' => 'Logout',

];
