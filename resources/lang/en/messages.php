<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during .
    |
    */

    'welcome' => 'Welcome',
    'landing' => "Your Application's Landing Page.",
    'delete' => "Are you sure to delete this element?",


    'action' => 'action',
    'actions' => "actions",
    'sections' => "sections",
    'section' => 'section',
    'subsections' => "subsections",
    'subsection' => 'subsection',
    'role' => 'role',
    'roles' => 'roles',
    'user' => 'user',
    'users' => 'users',
    'institution' => 'institution',
    'institutions' => 'institutions',
    'division' => 'division',
    'divisions' => 'divisions',
    'room' => 'room',
    'rooms' => 'rooms',
    'modality' => 'modality',
    'modalities' => 'modalities',
    'equipment' => 'equipment',
    'referring' => 'referring',
    'referrings' => 'referrings',
    'configuration' => 'configuration',
    'configurations' => 'configurations',
    'patient-types' => 'patient types',
    'patient-type' => 'patient type',
    'ubication' => 'ubication',
    'ubications' => 'ubications',
    'source' => 'source',
    'sources' => 'sources',
    'procedure' => 'procedure',
    'procedures' => 'procedures',
    'template' => 'template',
    'templates' => 'templates',
    'consumable' => 'consumable',
    'consumables' => 'consumables',
    'suspendreason' => 'suspend reason',
    'suspendreasons' => 'suspend reasons',
    
    'password' => 'password',
    'lbl-password-confirmation' => 'Password Confirmation',
    'spn-reset-password' => 'Reset Password',
    'remember' => 'remember',
    'forgot-password' => 'Forgot your password?',
    'login' => 'login',
    'btn-send' => 'Send',
    'lbl-email' => 'Email',

    'success-add' => 'The :name was added',
    'error-add' => 'The :name cannot be added',
    'success-edit' => 'The :name was edited',
    'error-edit' => 'The :name cannot be edited',
    'success-delete' => 'The :name was deleted',
    'error-delete' => 'The :name cannot be deleted',

    'show' => 'show',
    'add' => 'add',
    'edit' => 'edit',
    'purge' => 'delete',

];
