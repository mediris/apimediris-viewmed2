@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>{{ ucfirst(trans('messages.sources')) }}</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('sources.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            @foreach($sources as $source)
                <tr>
                    <td>{{ $source->id }}</td>
                    <td>{{ $source->description }}</td>
                    <td><a href="{{ route('sources.show', [$source]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('sources.edit', [$source]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('sources.delete', [$source]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection