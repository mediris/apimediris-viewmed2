@extends('layouts.app')

@section('title','HOME')

@section('content')
<div class="row">
    <div class="col-lg-12 ">
        <div class="panel no-bd bd-3 panel-stat panel-carrousel">
            <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="/images/banner.jpg" alt="First Slide">
                    </div>
                    <div class="item">
                        <img src="/images/banner3.jpg" alt="Second Slide">
                    </div>
                    <div class="item">
                        <img src="/images/banner2.jpg" alt="Third Slide">
                    </div>
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>

        </div>
    </div>
</div>
<div class="row ">
    <div class="col-lg-12 ">
        <div class="panel no-bd bd-3 panel-stat">
            <div class="panel-body bg-white">
                <div class="row">
                    <div class="col-md-2 align-center">
                        <img src="/images/message_icon.png">
                    </div>
                    <div id="message_board" class="col-md-10">
                        <p><b>El día de hoy 13/03/2015</b> el sistema presentará un mantenimento, agradecemos su colaboración Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an.
                            Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit
                            cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-b-80 charts">
    <div class="col-md-4">
        <div class="panel bd-0">
            <div class="panel-heading no-bd bg-blue-m">
                <h3 class="panel-title">Estad&iacute;stica</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class=" leyenda1 text-left c-gray">Solicitudes pendientes</h4>
                        <h4 class=" leyenda2 text-left c-gray">Ordenes por realizar</h4>
                        <h4 class=" leyenda3 text-left c-gray">Ordenes por dictar</h4>
                        <h4 class=" leyenda4 text-left c-gray">Ordenes por transcribir</h4>
                        <h4 class=" leyenda5 text-left c-gray">Ordenes por aprobar</h4>
                    </div>
                    <div class="col-md-6">
                        <div id="donut-chart1" class="m-b-m30"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel bd-0">
            <div class="panel-heading no-bd bg-blue-m">
                <h3 class="panel-title">Marzo</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 align-center">
                        <h4 class=" leyenda1 text-left c-gray">Solicitudes admitidas</h4>
                        <h4 class=" leyenda2 text-left c-gray">Ordenes dictadas</h4>
                        <h4 class=" leyenda3 text-left c-gray">Ordenes aprobadas</h4>
                        <h4 class=" leyenda4 text-left c-gray">Ordenes realizadas</h4>
                        <h4 class=" leyenda5 text-left c-gray">Ordenes transcritas</h4>
                    </div>
                    <div class="col-md-6 align-center">
                        <div id="donut-chart2" class="m-b-m30"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel bd-0">
            <div class="panel-heading no-bd bg-blue-m">
                <h3 class="panel-title">Estad&iacute;stica personal</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 align-center">
                        <h4 class=" leyenda1 text-left c-gray">Solicitudes pendientes</h4>
                        <h4 class=" leyenda2 text-left c-gray">Ordenes por realizar </h4>
                        <h4 class=" leyenda3 text-left c-gray">Ordenes por dictar </h4>
                        <h4 class=" leyenda4 text-left c-gray">Ordenes por transcribir</h4>
                        <h4 class=" leyenda5 text-left c-gray">Ordenes por aprobar</h4>
                    </div>
                    <div class="col-md-6 align-center">
                        <div id="donut-chart3" class="m-b-m30"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
