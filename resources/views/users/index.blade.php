@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Users</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('users.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Actions</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->first_name }}</td>
                    <td><a href="{{ route('users.show', [$user]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('users.edit', [$user]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('users.delete', [$user]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection