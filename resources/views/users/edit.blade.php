@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('users.edit', [$user]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('administrative_ID') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Administrative ID</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="administrative_ID" value="{{ $user->administrative_ID }}">

                                    @if ($errors->has('administrative_ID'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is Active?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="active" value="1" {{ $user->isActive() }}>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Username</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="username" value="{{ $user->username }}">

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('telephone_number') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Telephone Number</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="telephone_number" value="{{ $user->telephone_number }}">

                                    @if ($errors->has('telephone_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telephone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('telephone_number_2') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Telephone Number 2</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="telephone_number_2" value="{{ $user->telephone_number_2 }}">

                                    @if ($errors->has('telephone_number_2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telephone_number_2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Cellphone Number</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="cellphone_number" value="{{ $user->cellphone_number  }}">

                                    @if ($errors->has('cellphone_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cellphone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cellphone_number_2') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Cellphone Number 2</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="cellphone_number_2" value="{{ $user->cellphone_number_2 }}">

                                    @if ($errors->has('cellphone_number_2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cellphone_number_2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('signature') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Signature</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="signature" value="{{ $user->signaturer }}">

                                    @if ($errors->has('signature'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('signature') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('additional_information') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Additional Information</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="additional_information">{{ $user->additional_information }}</textarea>

                                    @if ($errors->has('additional_information'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('additional_information') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Role</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="role_id">
                                        <option value="">Select</option>
                                        @foreach($roles as $role)
                                            @if($user->role_id == $role->id)
                                                <option selected value="{{ $role->id }}">{{ $role->name }}</option>
                                            @else
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('role_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('role_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('suffix_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Suffix</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="suffix_id">
                                        <option value="">Select</option>
                                        @foreach($suffixes as $suffix)
                                            @if($user->suffix_id == $suffix->id)
                                                <option value="{{ $suffix->id }}" selected>{{ $suffix->name }}</option>
                                            @else
                                                <option value="{{ $suffix->id }}">{{ $suffix->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('suffix_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('suffix_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('prefix_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Prefix</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="prefix_id">
                                        <option value="">Select</option>
                                        @foreach($prefixes as $prefix)
                                            @if($user->prefix_id == $prefix->id)
                                                <option value="{{ $prefix->id }}" selected>{{ $prefix->name }}</option>
                                            @else
                                                <option value="{{ $prefix->id }}">{{ $prefix->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('prefix_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('prefix_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('institutions') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Institutions</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="institutions[]" multiple>
                                        @foreach($institutions as $institution)
                                            @if($user->haveInstitution($institution->id))
                                                <option selected value="{{ $institution->id }}">{{ $institution->name }}</option>
                                            @else
                                                <option value="{{ $institution->id }}">{{ $institution->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('institutions'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('institutions') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>Edit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
