@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit a Referring</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('referrings.edit', [$referring]) }}">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="first_name" value="{{ $referring->first_name }}">

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" value="{{ $referring->last_name }}">

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('administrative_ID') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Administrative ID</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="administrative_ID" value="{{ $referring->administrative_ID }}">

                                    @if ($errors->has('administrative_ID'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('telephone_number') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Telephone Number</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="telephone_number" value="{{ $referring->telephone_number }}">

                                    @if ($errors->has('telephone_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telephone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('telephone_number_2') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Telephone Number 2</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="telephone_number_2" value="{{ $referring->telephone_number_2 }}">

                                    @if ($errors->has('telephone_number_2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telephone_number_2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Cellphone Number</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="cellphone_number" value="{{ $referring->cellphone_number }}">

                                    @if ($errors->has('cellphone_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cellphone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cellphone_number_2') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Cellphone Number 2</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="cellphone_number_2" value="{{ $referring->cellphone_number_2 }}">

                                    @if ($errors->has('cellphone_number_2'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cellphone_number_2') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ $referring->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="address">{{ $referring->address }}</textarea>

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">User</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="user_id">
                                        <option value="">Select</option>
                                        @foreach($users as $user)
                                            @if($referring->user_id == $user->id)
                                                <option value="{{ $user->id }}" selected>{{ $user->username }}</option>
                                            @else
                                                <option value="{{ $user->id }}">{{ $user->username }}</option>
                                            @endif

                                        @endforeach
                                    </select>

                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('suffix_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Suffix</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="suffix_id">
                                        <option value="">Select</option>
                                        @foreach($suffixes as $suffix)
                                                @if($referring->suffix_id == $suffix->id)
                                                    <option value="{{ $suffix->id }}" selected>{{ $suffix->name }}</option>
                                                @else
                                                    <option value="{{ $suffix->id }}">{{ $suffix->name }}</option>
                                                @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('suffix_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('suffix_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('prefix_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Prefix</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="prefix_id">
                                        <option value="">Select</option>
                                        @foreach($prefixes as $prefix)
                                            @if($referring->prefix_id == $prefix->id)
                                                <option value="{{ $prefix->id }}" selected>{{ $prefix->name }}</option>
                                            @else
                                                <option value="{{ $prefix->id }}">{{ $prefix->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('prefix_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('prefix_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('institutions') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Institutions</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="institutions[]" multiple>
                                        @foreach($institutions as $institution)
                                            @if($referring->haveInstitution($institution->id))
                                                <option selected value="{{ $institution->id }}">{{ $institution->name }}</option>
                                            @else
                                                <option value="{{ $institution->id }}">{{ $institution->name }}</option>
                                            @endif

                                        @endforeach
                                    </select>

                                    @if ($errors->has('institutions'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('institutions') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
