@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Referrings</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('referrings.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Actions</th>
            </tr>
            @foreach($referrings as $referring)
                <tr>
                    <td>{{ $referring->id }}</td>
                    <td>{{ $referring->first_name }}</td>
                    <td><a href="{{ route('referrings.show', [$referring]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('referrings.edit', [$referring]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('referrings.delete', [$referring]) }}" onclick="return confirm('{{ trans("referrings.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection