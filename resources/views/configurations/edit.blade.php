@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <h1>Edit a Configuration</h1>
        </div>

        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal col-md-8" action="{{ route('configurations.edit', [$configuration]) }}" method="post">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="institution_id" value="{{ $configuration->institution_id }}">


                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Appointment Sms</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="appointment_sms" value="1" {{ $configuration->isChecked($configuration->appointment_sms) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Appointment Sms Notification</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="appointment_sms_notification" value="{{ $configuration->appointment_sms_notification }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Results Sms</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="results_sms" value="1" {{ $configuration->isChecked($configuration->results_sms) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Results Email Auto</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="results_email_auto" value="1" {{ $configuration->isChecked($configuration->results_email_auto) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Appointment Email</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="appointment_email" value="1" {{ $configuration->isChecked($configuration->appointment_email) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birthday Email</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="birthday_email" value="1" {{ $configuration->isChecked($configuration->birthday_email) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birthday Expression</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="birthday_expression" value="{{ $configuration->birthday_expression }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Mammography Email</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="mammography_email" value="1" {{ $configuration->isChecked($configuration->mammography_email) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Mammography Sms</label>

                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" name="mammography_sms" value="1" {{ $configuration->isChecked($configuration->mammography_sms) }}>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_0</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_0" value="{{ $configuration->birad_0 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_1</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_1" value="{{ $configuration->birad_1 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_2</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_2" value="{{ $configuration->birad_2 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_3</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_3" value="{{ $configuration->birad_3 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_4</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_4" value="{{ $configuration->birad_4 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_5</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_5" value="{{ $configuration->birad_5 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_6</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_6" value="{{ $configuration->birad_6 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_7</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_7" value="{{ $configuration->birad_7 }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Birad_8</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="birad_8" value="{{ $configuration->birad_8 }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Mammography Expression</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="mammography_expression" value="{{ $configuration->mammography_expression }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Mammography Reminder</label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="mammography_reminder" value="{{ $configuration->mammography_reminder }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Appointment Sms Expression</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="appointment_sms_expression" value="{{ $configuration->appointment_sms_expression }}">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>


            </form>

        </div>

    </div>

@endsection