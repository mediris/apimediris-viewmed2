@extends('layouts.app')

@section('title', 'LOGIN')

@section('content')
    <div class="container" id="login-block">
        <div class="row">
            <div class="logo">
                <img src="/images/logo_mediris.png" alt="">
            </div>
            <div class="col-sm-6 col-md-6 col-sm-offset-3">
                <div class="login-box clearfix">
                    <!-- BEGIN ERROR BOX -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- END ERROR BOX -->
                    <div class="login-logo">
                            <img src="/images/mediris_icon.png" alt="Mediris Logo">
                    </div>
                    <div class="login-form ">
                        <form method="post" action="{{ url('/login') }}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for='email' class="m-t-20">{{ ucfirst(trans('messages.lbl-email')) }}</label>
                                <input type="email" name="email" id="email" class="input-field form-control user m-b-20 btn-style" value="{{ old('email') }}"/>
                            </div>
                            <div class="form-group">
                                <label for="password">{{ ucfirst(trans('messages.password')) }}</label>
                                <input type="password" id="password" name="password" class="input-field form-control password" />
                            </div>

                            <div class="row">
                                <label class="col-md-6 recordar chek linea_login">
                                    <input type="checkbox" name="remember">
                                    {{ ucfirst(trans('messages.remember')) }} {{ ucfirst(trans('messages.password')) }}
                                </label>
                                <div class="forgot">
                                    <a href="{{ url('/password/reset') }}" >{{ trans('messages.forgot-password') }}</a>
                                </div>
                            </div>
                           <div class="btn-caja"> <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left" href="home.html"><span class="ladda-label">{{ ucfirst(trans('messages.login')) }}</span></button></div>
                        </form>
                    </div>
                </div>
                <div class="social-login row">
                    <div class="fb-login col-lg-6 col-md-12 animated flipInX">
                        <a href="#" class="btn btn-facebook btn-block">Connect with <strong>Facebook</strong></a>
                    </div>
                    <div class="twit-login col-lg-6 col-md-12 animated flipInX">
                        <a href="#" class="btn btn-twitter btn-block">Connect with <strong>Twitter</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
