@extends('layouts.app')

@section('title', 'RESET PASSWORD')

@section('content')
<div class="container" id="login-block">
    <div class="row">
        <div class="logo">
            <a href="{{ url('/login') }}"><img src="/images/logo_mediris.png" alt="Login"></a>
        </div>
        <div class="col-sm-6 col-md-6 col-sm-offset-3">
            <div class="login-box clearfix">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <!-- BEGIN ERROR BOX -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- END ERROR BOX -->
                <div class="login-logo">
                    <img src="/images/mediris_icon.png" alt="MEDIRIS Logo">
                </div>
                <div class="login-form">
                    <form method="post" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for='email' class="m-t-20">{{ ucfirst(trans('messages.lbl-email')) }}</label>
                            <input type="email" name="email" id="email" class="input-field form-control user m-b-20 btn-style" value="{{ $email or old('email') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="password">{{ ucfirst(trans('messages.password')) }}</label>
                            <input type="password" id="password" name="password" class="input-field form-control password" />
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{ ucfirst(trans('messages.lbl-password-confirmation')) }}</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" class="input-field form-control password" />
                            <div class="btn-caja"> <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left" href="home.html"><span class="ladda-label">{{ ucfirst(trans('messages.spn-reset-password')) }}</span></button></div>
                        </div>
                    </form>
                </div>
            </div>
            <!--<div class="panel panel-default">
                <div class="panel-heading">Reseteo Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i>Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>-->
        </div>
    </div>
</div>
@endsection
