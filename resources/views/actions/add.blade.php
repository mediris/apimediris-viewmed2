@extends('layouts.app')

@section('content')

    <style>

        ul
        {
            margin-left:40px;
        }

    </style>

    <div class="container">

        <div class="row">
            <h1>Add an Action</h1>
        </div>

        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal col-md-8" action="{{ route('actions.add') }}" method="post">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Action Name</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" placeholder="Action Name" value="{{ old('name') }}">
                    </div>

                </div>

                <!--<div class="form-group">
                    <label for="section_id" class="col-sm-2 control-label">Section</label>

                    <div class="col-sm-10">
                        <select class="form-control" name="section_id">
                            <option value="">Select</option>
                            @foreach($sections as $section)
                                <option value="{{ $section->id }}">{{ $section->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>-->

                <div class="form-group">

                    <label for="parent_id" class="col-sm-2 control-label">Section</label>

                    <div class="col-sm-10">

                        <ul>
                            @foreach($roots as $root)
                                <li>

                                    <div class="radio">
                                        <label><input type="radio" name="section_id" value="{{ $root->id }}">{{ $root->name }}</label>
                                    </div>

                                    {{ $root->printChildsActions() }}
                                </li>
                            @endforeach

                        </ul>

                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection