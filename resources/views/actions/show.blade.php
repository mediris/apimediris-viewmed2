@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                    Action
                </div>
                <div class="card-block">
                    <h1 class="card-title">{{ strtoupper($action->name) }}</h1>
                    <p class="card-text">{{ $action->section->name }}</p>
                    <a href="#" class="btn">Edit</a>
                </div>
            </div>

        </div>

    </div>

@endsection