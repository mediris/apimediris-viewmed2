@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Divisions</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif


        <a href="{{ route('divisions.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Institution</th>
                <th>Actions</th>
            </tr>
            @foreach($divisions as $division)
                <tr>
                    <td>{{ $division->id }}</td>
                    <td>{{ $division->name }}</td>
                    <td>{{ $division->institution->name }}</td>
                    <td><a href="{{ route('divisions.show', [$division]) }}">Show</a> | <a href="{{ route('divisions.edit', [$division]) }}">Edit</a> | <a href="{{ route('divisions.delete', [$division]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">Delete</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection