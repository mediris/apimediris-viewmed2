@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add a Division</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('divisions.edit', [$division]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $division->name }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('institution_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Prefix</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="institution_id">
                                        <option value="">Select</option>
                                        @foreach($institutions as $institution)
                                            @if($institution->id == $division->institution_id)
                                                <option value="{{ $institution->id }}" selected>{{ $institution->name }}</option>
                                            @else
                                                <option value="{{ $institution->id }}">{{ $institution->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('institution_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('institution_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
