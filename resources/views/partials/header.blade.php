@if (!Auth::guest())
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="menu-medium" class="sidebar-toggle tooltips">
                    <img src="/images/menu.png" width="80%">
                </a>
                <a class="navbar-brand" href="/"></a>
            </div>
            <!--<div class="navbar-center">AVISOS PERMANENTES!! </div>-->
            <div class="navbar-collapse collapse">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right header-menu">
                    <!-- BEGIN CALENDARIO -->
                    <li  id="fecha-header" >
                        <img src="/images/fecha.png" alt="Fecha" width="25" class="p-r-5">
                            <span class="username c-text" >
                             <script>

                                 var mydate=new Date();
                                 var year=mydate.getYear();
                                 if (year < 1000)
                                     year+=1900;
                                 var day=mydate.getDay();
                                 var month=mydate.getMonth()+1;
                                 if (month<10)
                                     month="0"+month;
                                 var daym=mydate.getDate();
                                 if (daym<10)
                                     daym="0"+daym;
                                 document.write("<font color='1c3e4e' face='Open Sans'>"+daym+"/"+month+"/"+year+"</font>")

                             </script>
                        </span>

                    </li>

                    <!-- BEGIN HORA -->
                    <li  id="hora-header" >
                        <img src="/images/hora.png" alt="Hora" width="30" class="p-r-5 separador">
                            <span id="reloj" class="username c-text" >
                             <script type="text/javascript">
                                 function startTime(){
                                     today=new Date();
                                     h=today.getHours();
                                     m=today.getMinutes();
                                     s=today.getSeconds();
                                     m=checkTime(m);
                                     s=checkTime(s);
                                     document.getElementById('reloj').innerHTML=h+":"+m+":"+s;
                                     t=setTimeout('startTime()',500);}
                                 function checkTime(i)
                                 {if (i<10) {i="0" + i;}return i;}
                                 window.onload=function(){startTime();}
                             </script>
                        </span>

                    </li>
                    <!-- END HORA -->
                    <!-- BEGIN USER -->
                    <li  id="user-header">

                            <img src="/images/avatar.png" alt="user avatar" width="30" class="p-r-5 separador">
                            <span class="username c-text">{{ ucfirst(Auth::user()->first_name) }} {{  ucfirst(Auth::user()->last_name) }}</span>

                    </li>
                    <!-- END USER  -->
                    <!-- BEGIN INSTITUCION -->
                    <li  id="messages-header" >
                            <span class="username c-text separador"  >
                             Centro Médico Docente La Trinidad
                        </span>

                    </li>
                    <!-- END INSTITUCION  -->
                    <!-- BEGIN CONFIGURACION -->
                    <li class="dropdown" id="config-header" >
                        <!--<a href="#" class="dropdown-toggle c-text " data-toggle="dropdown" data-hover="dropdown"  data-close-others="true">-->
                        <a href="#" class="dropdown-toggle c-text " data-toggle="dropdownCustom" data-hover="dropdown"  data-close-others="true">
                            <img src="/images/config.png" alt="configuracion" width="25" class="p-r-5">
                        </a>
                        <ul class="dropdown-menu" style="border-bottom:4px solid #0079C2">
                            <li id="clave" class="col-lg-6" >
                                <p class="m-b-20 form-margin"><strong>Cambio de Contraseña</strong></p>

                                <form action="{{ route('users.password', [Auth::user()]) }}" method="post">
                                    {!! csrf_field() !!}

                                    @if ($errors->has('actual'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('actual') }}</strong>
                                    </span>
                                    @endif
                                    <input class="form-control form-margin" type="password" placeholder="Actual contraseña" name="actual">
                                    <br>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    <input class="form-control form-margin" type="password" placeholder="Nueva contraseña" name="password">
                                    <br>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                    <input class="form-control form-margin" type="text" placeholder="Repetir contraseña" name="password_confirmation">
                                    <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left"><span class="ladda-label">Cambiar</span></button>

                                </form>

                            </li>
                            <li class="col-lg-6" style="border-left:1px solid #DFE5E9;">
                                <p class="m-b-20 form-margin"><strong>Institucion</strong></p>
                                <div class="col-md-12 m-b-20">
                                    <select class="form-control">
                                        <option>Centro Médico Docente La Trinidad</option>
                                        <option>Centro 2</option>
                                        <option>Centro 3</option>
                                    </select>
                                    <br>
                                    <p class="m-b-20 "><strong>Impresora</strong></p>
                                    <select class="form-control">
                                        <option>Impresora 1</option>
                                        <option>Impresora 2</option>
                                        <option>Impresora 3</option>
                                    </select>
                                    <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left"><span class="ladda-label">Cambiar</span></button>
                                </div>

                            </li>

                            <li class="dropdown-footer clearfix col-lg-12">
                                <a href="javascript:;" class="toggle_fullscreen icon-config" title="Fullscreen">
                                    <i class="glyph-icon flaticon-fullscreen3"></i>
                                </a>
                                <a href="lockscreen.html" class="toggle_fullscreen icon-config" title="Lock Screen">
                                    <i class="glyph-icon flaticon-padlock23"></i>
                                </a>
                                <a href="{{ url('/logout') }}" class="toggle_fullscreen icon-config" title="Logout">
                                    <i class="fa fa-power-off"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END  CONFIGURACION -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
    </nav>
@endif