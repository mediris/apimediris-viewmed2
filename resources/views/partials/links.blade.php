<link rel="shortcut icon" href="{{ '/images/mediris_favicon.png' }}">
<link href="{{ '/css/icons/icons.min.css' }}" rel="stylesheet">
<link href="{{ '/css/bootstrap.min.css' }}" rel="stylesheet">
<link href="{{ '/css/plugins.min.css' }}" rel="stylesheet">
<link href="{{ '/plugins/bootstrap-loading/lada.min.css' }}" rel="stylesheet">
<link href="{{ '/css/style.min.css' }}" rel="stylesheet">
<link href="#" rel="stylesheet" id="theme-color">
<link href="{{ '/plugins/charts-d3/nv.d3.css' }}"  rel="stylesheet">
<link href="{{ '/css/style.css' }}" rel="stylesheet">
<link href="{{ '/plugins/revolution/revolution.css' }}" rel="stylesheet">
<link href="{{ '/css/logoscorp.css' }}" rel="stylesheet">



<!-- Fonts -->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

<!-- Styles -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
{{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

<script src="{{ 'plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js' }}"></script>