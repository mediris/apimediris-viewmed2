@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Rooms</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif


        <a href="{{ route('rooms.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Division</th>
                <th>Institution</th>
                <th>Actions</th>
            </tr>
            @foreach($rooms as $room)
                <tr>
                    <td>{{ $room->id }}</td>
                    <td>{{ $room->name }}</td>
                    <td>{{ $room->division->name }}</td>
                    <td>{{ $room->division->institution->name }}</td>
                    <td><a href="{{ route('rooms.show', [$room]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('rooms.edit', [$room]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('rooms.delete', [$room]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection