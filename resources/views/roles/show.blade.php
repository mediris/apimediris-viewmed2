@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                    Role
                </div>
                <div class="card-block">
                    <h1 class="card-title">{{ strtoupper($role->name) }}</h1>
                    <h2>Actions</h2>
                    @foreach($sections as $section)

                        <h3>{{ $section->name }}</h3>

                        @foreach($role->actions as $action)

                            <ul>
                                @if($section->id == $action->id)
                                    <li>{{ $action->name }}</li>
                                @endif
                            </ul>



                        @endforeach

                    @endforeach

                    <a href="#" class="btn">Edit</a>
                </div>
            </div>

        </div>

    </div>

@endsection