@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('messages.template') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('templates.edit', [$template]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="description" value="{{ $template->description }}">

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is Active?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="active" value="1" {{ $template->active == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('template') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Template</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="template">{{ $template->template }}</textarea>

                                    @if ($errors->has('template'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('template') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('procedures') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Procedures</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="procedures[]" multiple>
                                        @foreach($procedures as $procedure)
                                            @if($template->hasProcedure($procedure->id))
                                                <option value="{{ $procedure->id }}" selected>{{ $procedure->name }}</option>
                                            @else
                                                <option value="{{ $procedure->id }}">{{ $procedure->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('procedures'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('procedures') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ ucfirst(trans('messages.edit')) }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
