<?php

use Illuminate\Database\Seeder;
use App\PlatesSize;

class PlatesSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = ['N/A', '2x2', '5x5', '3x2'];

        foreach($sizes as $size)
        {
            PlatesSize::create([
                'size' => $size,
            ]);
        }
    }
}