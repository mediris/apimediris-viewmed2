<?php

use Illuminate\Database\Seeder;
use App\Source;

class SourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sources = [
            'iniciativa propia',
            'control de mamografía',
            'orden médica',
            'post operados',
            'medios propios',
            'sanitas',
            'convenio',
            'hospital público',
            'clinica privada',
            'dependencia cmdlt',
            'otro',
            'emergencia adulto',
            'emergencia pediatrica',
            'pdvsa',
            'bmi',
            'planisalud',
            'oriental de seguros',
            'sara ott',
            'cantv',
            'samhoi',
            'multinacional',
            'parsalud',
            'seguros caracas'
        ];

        foreach($sources as $source)
        {
            Source::create([
                'description' => $source,
                'active' => 1
            ]);
        }
    }
}
