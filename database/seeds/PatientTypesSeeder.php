<?php

use Illuminate\Database\Seeder;
use App\PatientType;

class PatientTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patientTypes = [
            [0, 'ambulatorio', 1],
            [0, 'convenio', 1],
                [1, 'plan od', 1],
            [0, 'emergencia', 1],
                [1, 'quirofano', 1],
                [1, 'uci adulto', 1],
                [1, 'uci neonatal', 1],
                [1, 'uci pediatrico', 1],
            [0, 'hospitalización', 1],
                [1, 'citas', 1],
                [1, 'recuperación', 1],
                [1, 'retén', 1],
                [1, 'senos ayuda', 1],
                [1, 'senos salud', 1],
                [1, 'trauma shock', 1],
        ];

        $icons = [
            '&#xf0f3',
            '&#xf1ad',
            '&#xf1fd',
            '&#xf1ec',
            '&#xf030',
            '&#xf1b9',
            '&#xf13d',
            '&#xf249',
            '&#xf291',
            '&#xf073',
            '&#xf015',
            '&#xf017',
            '&#xf111',
            '&#xf0f2',
            '&#xf024',
            '&#xf07a',
            '&#xf013',
            '&#xf109',
            '&#xf0f4',
            '&#xf0c4',
            '&#xf1c0',
            '&#xf08b',
            '&#xf1e6',
            '&#xf0e0',
            '&#xf06e',
            '&#xf222',
            '&#xf221',
            '&#xf1e5',
            '&#xf07b',
            '&#xf07c',
            '&#xf007',
            '&#xf0c0',
            '&#xf234',
            '&#xf235',
            '&#xf254',
            '&#xf084',
            '&#xf02d',
            '&#xf0eb',
            '&#xf0d1',
            '&#xf26c',
            '&#xf040',
        ];

        $i = 1;
        $prevId = null;
        foreach($patientTypes as $arrPatientType)
        {
            $patientTypeParent = $arrPatientType[0];
            if ($patientTypeParent == 0) {
                $patientTypeParent = null;
            } else {
                $patientTypeParent = $prevId; 
            }

            $patientType = $arrPatientType[1];
            $requireAproval = $arrPatientType[2];

            $result = PatientType::create([
                'description'       => $patientType,
                'administrative_ID' => $i,
                'priority'          => rand(1, 10),
                'parent_id'         => $patientTypeParent,
                'level'             => 0,
                'active'            => 1,
                'icon'              => $icons[rand(1, 20)],
                'admin_aprob'       => $requireAproval,
                'sms_send'          => 1,
                'email_patient'     => 1,
                'email_refeer'      => 1
            ]);

            if ( $patientTypeParent == null ) {
                $prevId = $result->id;
            }

            $i++;
        }
    }
}
