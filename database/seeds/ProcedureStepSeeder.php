<?php

use Illuminate\Database\Seeder;
use App\Step;
use App\Procedure;

class ProcedureStepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // se carga el seed a partir del archivo procedimiento_posible.csv
        Excel::load('database/exportFile/procedimiento-pasos.csv', function ( $reader ){
            foreach( $reader->get() as $seed ){
                // se obtiene el procedure especifico a traves del campo oldId
                $procedure = Procedure::where('oldId', $seed->id_procedimiento)->get();
                // se obtiene el step especifico a traves del campo Id
                $step = Step::where('id', $seed->id_paso)->get();
                if( ( isset( $procedure[0] ) ) && ( isset( $step[0] ) ) ){
                    /* se incluye en la tabla procedure_step el id del step y el id del procedure, con el fin
                    de relacionar las tablas Procedures y Step a traves de la tabla procedure_step */
                    Step::find($step[0]->id)->procedures()->attach($procedure[0]->id, ['order' => 1]);
                }
            }
        });
    }
}
