<?php

use Illuminate\Database\Seeder;
use App\PregnancyStatus;

class PregnancyStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['not-pregnant', 'possibly-pregnant', 'definitely-pregnant', 'unknown', 'na'];

        foreach($statuses as $status)
        {
            PregnancyStatus::create([
                'name' => $status,
            ]);
        }
    }
}
