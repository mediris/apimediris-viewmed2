<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = ['cm', 'cc', 'cm3', 'gr', 'par'];

        foreach($units as $unit)
        {
            Unit::create([
                'active' => 1,
                'description' => $unit,
            ]);
        }
    }
}
