<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            '-',
            'Craneo y Contenido', 
            'Cara', 
            'Mastoids y Cuello', 
            'Columna y Contenido', 
            'Sist. esquelético y partes blandas', 
            'Corazón y grandes Vasos',
            'Pulmón, Mediastino, Pleura',
            'Sist. Gastrointestinal',
            'Sist. Genitourinario',
            'Sist. Vascualar y Unfático',
            'Mama',
        ];
        
        foreach($categories as $category)
        {
            Category::create([
                'active' => 1,
                'description' => $category,
                'language' => 'es'
            ]);
        }
    }
}
