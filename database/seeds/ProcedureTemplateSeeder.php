<?php

use Illuminate\Database\Seeder;
use App\Template;
use App\Procedure;
use Maatwebsite\Excel\Facades\Excel;

class ProcedureTemplateSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        // se carga el seed a partir del archivo modalidad.csv
        Excel::load('database/exportFile/procedimiento_plantilla.csv', function ( $reader ){
            foreach( $reader->get() as $seed ){
                // se obtiene el procedure especifico a traves del campo oldId
                $procedure = Procedure::where('oldId', $seed->id_procedimiento)->get();
                // se obtiene el template especifico a traves del campo oldId
                $template = Template::where('oldId', $seed->id_plantilla)->get();
                if( ( isset( $procedure[0] ) ) && ( isset( $template[0] ) ) ){
                    /* se incluuye en la tabla procedure_template el id del template y el id del procedure, con el fin
                    de relacionar las tablas Procedures y Templates a traves de la tabla prodcedure_template  */
                    Template::find($template[0]->id)->procedures()->attach($procedure[0]->id);
                }
            }
        });

    }
}
