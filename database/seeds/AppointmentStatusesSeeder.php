<?php

use Illuminate\Database\Seeder;
use App\AppointmentStatus;

class AppointmentStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        //$statuses       = ['created',   'scheduled',    'admitted',     'discharged'];
        //$status_color   = ['yellow',    'yellow',       'green',        'red'];
        $statuses       = ['created',   'noshow',   'admitted', 'deleted', 'rejected'];
        $status_color   = ['yellow',    'yellow',   'green',    'red',     'red'];

        foreach($statuses as $key => $status)
        {
            AppointmentStatus::create([
                'description' => $status,
                'status_color' => $status_color[$key]
            ]);
        }
    }
}
