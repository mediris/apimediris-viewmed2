<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Step;

class StepsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // JCH - 06/10/2017 - Se desincorpora por lectura al archivo CSV
        /*$faker = Faker::create();
        
        for($i = 1; $i <= 30; $i++)
        {
            Step::create([
                'active' => true,
                'administrative_ID' => 'STEP_'.$i,
                'description' => 'Paso '.$i,
                'indications' => $faker->text(300)
            ]);

            // se llena la tabla pivote equipment_step
            //Step::find($i)->equipment()->attach([rand(1, 8)]);
        }*/

        // se carga el seed a partir del archivo pasos.csv
        Excel::load('database/exportFile/pasos.csv', function($reader) {

            foreach ($reader->get() as $seed) {
                if ($seed->activo == 1)
                    Step::create([
                        'active' => $seed->activo,
                        'administrative_ID' => $seed->id_administrativo,
                        'description' => $seed->descripcion,
                        'indications' => $seed->indicaciones
                    ]);
            }
        });

    }
}
