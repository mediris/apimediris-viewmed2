<?php

use Illuminate\Database\Seeder;
use App\Modality;
use Maatwebsite\Excel\Facades\Excel;

class ModalitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // se carga el seed a partir del archivo modalidad.csv
        Excel::load('database/exportFile/modalidad.csv', function($reader) {

            foreach ($reader->get() as $seed) {
                Modality::create([
                    'active' => 1,
                    'name' => $seed->descripcion,
                    'parent_id' => 0,
                    'level' => 0,
                    'requires_additional_fields' => $seed->requiresadditionalfields,
                ]);
            }
        });
    }
}
