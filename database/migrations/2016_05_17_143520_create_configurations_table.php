<?php
/**
 * @fecha 15-06-2017
 * @programador Hendember Heras
 * @change: Se eliminaron los campos birad_0 .. birad_8 de esta tabla que indicaban la frecuencia de envío de notificaciones de examenes biradas,
            Estos campos ahora se encuentran en la tabla bi_rads en el campo frecuencia.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('appointment_sms');                                         //indica si se envían los sms de recordatorio de citas.
            $table->integer('appointment_sms_template_id')->nullable()->unsigned();     //plantilla que se usa para el envío de sms del campo anterior
            $table->integer('appointment_sms_notification');                            //indica con qué anticipación (en días) se envía el recordatorio al paciente de su cita.
            
            $table->boolean('appointment_email');                                       //indica si en el momento que se crea un cita se le envía un correo al paciente con el detalle de esta.
            $table->integer('appointment_email_template_id')->nullable()->unsigned();   //plantilla que se usa para el envío de email del campo anterior

            $table->boolean('results_sms');                                             //indica si se envía una notificación por sms cuando el estudio se termina.
            $table->integer('results_sms_template_id')->nullable()->unsigned();         //plantilla que se usa para el envío de sms del campo anterior

            $table->boolean('results_email_auto');                                        //indica si los resultados (el informe) se envía de forma automática al paciente cuando el estudio se termina.
            $table->integer('results_email_auto_template_id')->nullable()->unsigned();    //plantilla que se usa para el envío de email al paciente de los resultados (campo anterior)
            $table->integer('results_email_referer_template_id')->nullable()->unsigned(); //plantilla que se usa para el envío de email al referente de los resultados (campo anterior)

            $table->boolean('birthday_email');                                          //indica si el día del cumpleaños del paciente se envía un correo.
            $table->string('birthday_expression', 45);
            
            $table->boolean('mammography_sms');                                         //indica si se envían sms para notificar a los pacientes que se acerca la fecha de control de mamografía
            $table->integer('mammography_sms_template_id')->nullable()->unsigned();     //plantilla que se usa para el envío de sms del campo anterior

            $table->boolean('mammography_email');                                       //indica si se envían correos para notificar a los pacientes que se acerca la fecha de control de mamografía
            $table->integer('mammography_email_template_id')->nullable()->unsigned();   //plantilla que se usa para el envío de email del campo anterior


            $table->string('mammography_expression', 45);
            $table->integer('mammography_reminder');                                    //Indica con qué anticipación (en meses) antes de que se cumpla el tiempo de su último control debo notificar a la paciente. Es decir, si yo me realicé una mamografía con categoría BI-RADS 1 en junio de 2015, el primero lunes de mayo de 2016 me llegará un recordatorio de que se acerca mi control, así a la paciente le da tiempo de hacer su cita.
            $table->string('appointment_sms_expression', 45);

            $table->boolean('results_email');                                           //indica si en el momento que se genera el resultado final (informe o addendum) correo al paciente con el detalle de esta.
            $table->integer('results_email_template_id')->nullable()->unsigned();       //plantilla que se usa para el envío de email del campo anterior

            $table->boolean('active_poll');                                            //indica si se encuentra habilitada el url para envio de encuentas
            $table->string('url_poll',90)->nullable();                                  //indica la URL para ser enviada en la encuenta
            $table->integer('poll_email_template_id')->nullable()->unsigned();       //plantilla que se usa para el envío de email del campo anterior

            $table->timestamps();

            /*Todos los campos que son _expression
            son una CRON expression que indican cuándo se ejecutan las rutinas que envían los mensajes (email o sms) para cada caso.
            Por ejemplo, para el caso de la mamografía, se envía el mensaje el primer lunes de cada mes,
            por lo tanto mammography_expression sería 0 0 7 ? 1/1 MON#1 * .
            Mientras que los sms de las citas se envían todos los días (hábiles) a las 9 a. m.
            recordándole al paciente que tienen una cita al día siguiente: 0 0 9 ? * MON-FRI.
            Esto se cambia fácilmente modificando estas expresiones.
            Nota: Para cada caso se usa una plantilla, pero eso ya lo veremos.*/

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
