<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddendumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addendums', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->integer('user_id')->index()->unsigned();
            $table->integer('requested_procedure_id')->index()->unsigned();
            $table->string('user_name')->index();
            $table->dateTime('date')->index();
            $table->text('text');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addendums');
    }
}
