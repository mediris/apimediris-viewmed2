<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('dictate_date');
            $table->dateTime('transcription_date');
            $table->dateTime('approbation_date');
            $table->integer('approbation_user_id');
            $table->integer('radiologist_user_id');
            $table->integer('transcriptor_user_id');
            $table->string('file_path', 60);
            $table->text('text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
