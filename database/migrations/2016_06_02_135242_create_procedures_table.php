<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedures', function (Blueprint $table) {

            $table->increments('id');
            $table->string('description', 50)->index();
            $table->boolean('active');
            $table->boolean('interview');
            $table->boolean('mammography');
            $table->boolean('worklist');
            $table->boolean('images');
            $table->boolean('radiologist');
            $table->float('radiologist_fee', 16, 2);
            $table->boolean('technician');
            $table->float('technician_fee', 16, 2);
            $table->float('transcriptor_fee', 16, 2);
            $table->string('administrative_ID', 45)->unique()->index();
            $table->text('indications');
            $table->integer('modality_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedures');
    }
}
