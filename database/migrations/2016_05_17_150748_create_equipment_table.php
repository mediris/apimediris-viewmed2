<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment', function (Blueprint $table) {

            $table->increments('id');
            $table->string('ae_title', 50)->unique()->index();
            $table->string('name', 50)->index();
            $table->boolean('active');
            //$table->integer('room_id')->unsigned()->index();
            //$table->integer('modality_id')->unsigned()->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment');
    }
}
