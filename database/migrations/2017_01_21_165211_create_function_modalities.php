<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionModalities extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        DB::unprepared('
        CREATE FUNCTION `MODALITIES`(id INT) RETURNS varchar(500) CHARSET latin1
        BEGIN
        
        DECLARE Modalities VARCHAR (500);
        
        SET Modalities = (SELECT GROUP_CONCAT(DISTINCT modalityName) FROM procedurestepsmods WHERE procedureID = id GROUP BY procedureID);
        
        RETURN Modalities;
        
        END
        ');
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        DB::unprepared('DROP FUNCTION IF EXISTS Modalities');
    }
}