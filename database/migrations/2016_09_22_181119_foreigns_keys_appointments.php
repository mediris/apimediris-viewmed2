<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysAppointments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {

            $table->foreign('appointment_status_id')->references('id')->on('appointment_statuses')->onDelete('restrict');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('restrict');
            $table->foreign('procedure_id')->references('id')->on('procedures')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {

            $table->dropForeign('appointments_appointment_status_id_foreign');
            $table->dropForeign('appointments_room_id_foreign');
            $table->dropForeign('appointments_procedure_id_foreign');
        });
    }
}
