<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysOrderProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_procedure', function (Blueprint $table) {

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('procedure_id')->references('id')->on('procedures')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_procedure', function (Blueprint $table) {
            
            $table->dropForeign('order_procedure_order_id_foreign');
            $table->dropForeign('order_procedure_procedure_id_foreign');

        });
    }
}
