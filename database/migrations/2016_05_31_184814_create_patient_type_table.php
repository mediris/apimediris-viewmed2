<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_types', function (Blueprint $table) {

            $table->increments('id');
            $table->string('description', 25)->unique()->index();
            $table->string('administrative_ID', 2)->unique()->index();
            $table->integer('priority')->index();
            $table->integer('parent_id')->nullable()->unsigned()->index();
            $table->integer('level');
            $table->boolean('active');
            $table->string('icon', 10);
            $table->boolean('admin_aprob');
            $table->boolean('sms_send');
            $table->boolean('email_patient');
            $table->boolean('email_refeer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_types');
    }
}
