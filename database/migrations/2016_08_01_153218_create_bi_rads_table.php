<?php
/**
 * @fecha 15-06-2017
 * @programador Hendember Heras
 * @change: Se agregó el campo "frecuency" que indica cada cuantos meses se debe enviar la notificación de recordatorio de examen.
            Este campo se guardaba en la tabla configuración pero no había relación foranea con esta tabla.
            El comentario siguiente es la explicación de como funciona.
 */

/*birad_0…8 Los BI-RADS son las categorías que utilizan los médicos para los resultados de estudios mamográficos.
            BI-RADS 0:Evaluación incompleta,
            BI-RADS 1:Negativo (sin anomalía),
            BI-RADS 2: Hallazgo benigno… y así.
            Realmente son: 0, 1, 2, 3, 4A, 4B, 4C, 5, 6 (9 en total).
            Según esa categoría asignada a un estudio, los controles deben ser más o menos frecuentes.
            Por ejemplo, una paciente con una mamografía de categoría 1 debe hacerse controles anuales (lo común),
            pero una paciente con una mamografía de categoría 3 debe hacerse controles cada 6 meses.
            Esos campos en la tabla indican, en meses, la frecuencia con la que se debe notificar a la paciente,
            cada 6 meses, cada 12 meses… para cada categoría.*/

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiRadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bi_rads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 25);
            $table->integer('frecuency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bi_rads');
    }
}
