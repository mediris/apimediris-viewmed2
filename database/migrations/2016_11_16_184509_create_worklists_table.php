<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('mysqlwl')->hasTable('orden')) {
            Schema::connection('mysqlwl')->create('orden', function (Blueprint $table) {
                $table->increments('id');
                $table->string('aetitle', 255);
                $table->string('procsetpstartdate', 255);
                $table->string('procsetpstartdatetime', 255);
                $table->string('modality', 255);
                $table->string('refp', 255); //performing physician
                $table->string('psd', 255); //procedure step desc
                $table->string('psid', 255); //procedure step id
                $table->string('rpid', 255); //requested procedure id
                $table->string('rpd', 255); //requested procedure desc
                $table->string('siuid', 255); //Study instance uid
                $table->string('an', 255); //accession
                $table->string('rp', 255); //requested procedure
                $table->string('rpn', 255); //referring physician
                $table->string('patn', 255); //Patient name
                $table->string('pid', 255); //Patient id
                $table->string('pabd', 255); //Patient birthdate
                $table->string('ps', 255); //patient sex
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysqlwl')->dropIfExists('orden');
    }
}
