<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BiRad;
use App\Http\Requests;

class BiRadsController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de BiRads.
     */
    public function index(Request $request)
    {
        try
        {
            $biRads = BiRad::all();
            return $biRads;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: biRads. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

     public function show(BiRad $birad, Request $request) {
        return $birad;
    }
}
