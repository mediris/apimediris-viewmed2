<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\BiRad;
use DB;
use Illuminate\Database\Query\Builder;

use DateTime;
use DatePeriod;
use DateIntercal;

class ReportsController extends Controller{

	public function __construct () {
            
        $this->modalities = "";
		$this->procedure = "";
		$this->patientType = "";
		$this->requestedProcedureStatus = "";
		$this->users = "";
		$this->appointmentStatus = "";
		$this->rooms = "";

    }

    public function index(Request $request, $name) {

    	$data = $request->all();
		$result = "";

		if($name == 'Birads')
			$result = $this->reportBirads($data);
		elseif($name == 'Listado-Ordenes')
			$result = $this->reportList($data);
		elseif($name == 'Honorario-Radiologo' || $name == 'Honorario-Tecnico' || $name == 'Honorario-Transcriptor')
			$result = $this->reportHonorary($data);
		elseif($name == 'Listado-Citas')
			$result = $this->reportAppointment($data);
		elseif($name == 'Ordenes-Por-Dictar' || $name == 'Ordenes-Por-Aprobar' || $name == 'Ordenes-Por-Transcribir')
			$result = $this->reportOrderFor($data);		
		elseif($name == 'Ordenes-Dictadas' || $name == 'Ordenes-Aprobadas' || $name == 'Ordenes-Transcritas' || $name == 'Ordenes-Realizadas')
			$result = $this->reportOrders($data);
		elseif($name == 'Paciente-Por-Genero')
			$result = $this->reportGender($data);
		elseif($name == 'Eficiencia-Ordenes-Terminadas')
			$result = $this->reportEfficient($data);
		elseif ($name == 'Transcripcion')
			$result = $this->reportTranscription($data);
		elseif($name == 'countOrderHonorario')
			$result = $this->countOrderHonorary($data);
		elseif($name == 'countOrderCitas')
			$result = $this->countOrderAppointment($data);
		elseif($name == 'countOrdersFor')
			$result = $this->countOrdersFor($data);
		elseif($name == 'countOrders')
			$result = $this->countOrders($data);
		elseif($name == 'countEfficient')
			$result = $this->countEfficient($data);
		return $result;	
    }

	private function getDataArray($data,$name){
		
		if(isset($data['data'][$name])){
			foreach($data['data'][$name] as $key => $value){
				if($key == 0)
					$this->$name = $value;
				else
					$this->$name .= "," . $value;
			}
		}
	}

	private function reportBirads($data){

		$this->getDataArray($data,"patientType");
		//$this->getDataArray($data,"users");
		
		$colums = array("name","total","positives","negatives","unknowns");
		$types = array("name VARCHAR(50)","total INT","positives INT","negatives INT","unknowns INT");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
					->select(DB::raw("
						bi_rads.description AS `BIRADS`,
						COUNT(bi_rads.id) AS `ORDERS`,
						COUNT(CASE WHEN requested_procedures.biopsy_result = 0 THEN 1 ELSE NULL END) AS `POSITIVAS`,
						COUNT(CASE WHEN requested_procedures.biopsy_result = 1 THEN 1 ELSE NULL END) AS `NEGATIVAS`,
						COUNT(CASE WHEN requested_procedures.biopsy_result = 2 THEN 1 ELSE NULL END) AS `DESCONOCIDAS`"))
					->leftJoin('bi_rads','requested_procedures.bi_rad_id','=','bi_rads.id')
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
					->join(	DB::raw("( SELECT service_requests.id
							FROM patient_types, service_requests
							WHERE patient_types.id IN ( " . $this->patientType . " ) AND 
							patient_types.id = service_requests.patient_type_id ) AS services"), function($join){
								$join->on('services.id','=','requested_procedures.service_request_id');
							})
					->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
					//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                    ->whereIn('procedures.modality_id',$data['data']['modalities'])
					->where('requested_procedures.bi_rad_id','!=','999')
                    ->whereIn('requested_procedures.radiologist_user_id', $data['data']['users'])
					->whereBetween('requested_procedures.created_at',[$date1, $date2])
					->groupBy('bi_rads.description')
					->get();
                    //->toSql();

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function reportList($data){

        //$colums = array("orderId","patientId","patientDescription","procedureName","procedureModality","procedureStatus", "referring", "procedureService","procedurePreadmision","procedureTechnician","procedureDictation","procedureTranscription","procedureApproval","procedureDeliver");
        //$types = array("orderId VARCHAR(50)","patientId VARCHAR(50)","patientDescription VARCHAR(50)","procedureName VARCHAR(50)","procedureModality VARCHAR(50)","procedureStatus VARCHAR(50)","referring VARCHAR(50)","procedureService VARCHAR(50)","procedurePreadmision VARCHAR(50)","procedureTechnician VARCHAR(50)","procedureDictation VARCHAR(50)","procedureTranscription VARCHAR(50)","procedureApproval VARCHAR(50)","procedureDeliver VARCHAR(50)");

        $colums = array("orderId","patientId","patientDescription","procedureName","procedureModality","procedureStatus","referring","procedureService",
            "diffprocedureService", "procedurePreadmision","diffprocedurePreadmision","procedureTechnician","diffprocedureTechnician","procedureDictation",
            "diffprocedureDictation","procedureTranscription","diffprocedureTranscription","procedureApproval","diffprocedureApproval","procedureDeliver");
        $types = array("orderId VARCHAR(50)","patientId VARCHAR(50)","patientDescription VARCHAR(50)","procedureName VARCHAR(50)","procedureModality VARCHAR(50)",
            "procedureStatus VARCHAR(50)","referring VARCHAR(50)","procedureService VARCHAR(50)","diffprocedureService VARCHAR(50)", "procedurePreadmision VARCHAR(50)",
            "diffprocedurePreadmision VARCHAR(50)","procedureTechnician VARCHAR(50)","diffprocedureTechnician VARCHAR(50)","procedureDictation VARCHAR(50)",
            "diffprocedureDictation VARCHAR(50)","procedureTranscription VARCHAR(50)","diffprocedureTranscription VARCHAR(50)","procedureApproval VARCHAR(50)",
            "diffprocedureApproval VARCHAR(50)","procedureDeliver VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

        $result = DB::table('requested_procedures')
            ->select( DB::raw("
                        requested_procedures.id AS `orderId`, service_requests.patient_identification_id AS `patientId`, 
                        patient_types.description AS `patientDescription`, procedures.description AS `procedureName`, 
                        modalities.name AS `procedureModality`, requested_procedure_statuses.description AS `procedureStatus`,
                        CONCAT(referrings.first_name,' ',referrings.last_name) AS `referring`,
                        requested_procedures.created_at AS `procedureService`,
                        SEC_TO_TIME(TIMESTAMPDIFF(second, service_requests.created_at, (IFNULL(procedures_status_log.Preadmision,service_requests.created_at)))) as diffprocedureService,
                        (IFNULL(procedures_status_log.Preadmision,service_requests.created_at)) AS `procedurePreadmision`,
                        SEC_TO_TIME(TIMESTAMPDIFF(second, (IFNULL(procedures_status_log.Preadmision,service_requests.created_at)), requested_procedures.technician_end_date)) as diffprocedurePreadmision,
                        requested_procedures.technician_end_date AS `procedureTechnician`,
                        SEC_TO_TIME(TIMESTAMPDIFF(second, requested_procedures.technician_end_date, requested_procedures.dictation_date)) as diffprocedureTechnician,
                        requested_procedures.dictation_date AS `procedureDictation`,
                        SEC_TO_TIME(TIMESTAMPDIFF(second, requested_procedures.dictation_date, requested_procedures.transcription_date)) as diffprocedureDictation,
                        requested_procedures.transcription_date AS `procedureTranscription`,
                        SEC_TO_TIME(TIMESTAMPDIFF(second, requested_procedures.transcription_date, requested_procedures.approval_date)) as diffprocedureTranscription,
                        requested_procedures.approval_date AS `procedureApproval`,
                        SEC_TO_TIME(TIMESTAMPDIFF(second, approval_date, procedureDeliver)) as diffprocedureApproval,
                        (IFNULL(`procedureDeliver`,'0000-00-00 00:00:00')) AS `procedureDeliver`"))
            ->join('procedures','procedures.id','=','requested_procedures.procedure_id')
            ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR 
            ->join('requested_procedure_statuses','requested_procedure_statuses.id','=','requested_procedures.requested_procedure_status_id')
            ->join('service_requests','service_requests.id','=','requested_procedures.service_request_id')
            ->join('patient_types','patient_types.id','=','service_requests.patient_type_id')
            ->join('referrings','referring_id','=','referrings.id')
            ->leftJoin( DB::raw("( SELECT id, requested_procedure_id, date AS `procedureDeliver` FROM delivers) AS deliver"), function($join){
                $join->on('requested_procedures.id','=','deliver.requested_procedure_id');
            })
            ->leftJoin( DB::raw("( SELECT requested_procedure_id, date as `Preadmision` FROM requested_procedures_status_log where old_status = 1 and new_status = 2) AS procedures_status_log"), function($join){
                $join->on('requested_procedures.id','=','procedures_status_log.requested_procedure_id');
            })
            ->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
            ->whereIn('modalities.id',$data['data']['modalities'])
            ->whereIn('requested_procedures.requested_procedure_status_id',$data['data']['requestedProcedureStatus'])
            ->whereIn('service_requests.patient_type_id',$data['data']['patientType'])
            ->whereIn('service_requests.referring_id',$data['data']['referring'])
            ->whereBetween('service_requests.created_at',[$date1, $date2])
            ->get();
            //->toSql();
        
		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function reportHonorary($data){

		$colums = array("history","request","patientType","orderNumber","procedureName","procedureAdmision","procedureDictation","procedureApproval","cost","userId");
		$types = array("history INT","request VARCHAR(50)","patientType VARCHAR(50)","orderNumber INT","procedureName VARCHAR(50)","procedureAdmision VARCHAR(50)","procedureDictation VARCHAR(50)","procedureApproval VARCHAR(50)","cost VARCHAR(50)","userId VARCHAR(50)");

		$userId = "";
		$admisionDate = "";
		$fee = "";
		$status = "";

		if($data['data']['type'] == 'Radiologo'){
			$userId = 'requested_procedures.radiologist_user_id';
			$admisionDate = 'requested_procedures.approval_date';
			$fee = 'radiologist_fee';
			$status = [6];	// Orden con status Finish
		}elseif($data['data']['type'] == 'Transcriptor'){
			$userId = 'requested_procedures.transcriptor_user_id';
			$admisionDate = 'requested_procedures.transcription_date';
			$fee = 'transcriptor_fee';
			$status = [5, 6];	// Orden con status to_approve, finish
		}elseif($data['data']['type'] == 'Tecnico'){
			$userId = 'requested_procedures.technician_user_id';
			$admisionDate = 'requested_procedures.technician_end_date';
			$fee = 'technician_fee';
			$status = [3, 4, 5, 6];	// Orden con status to_dictate, to_transcriber, to_approve, finish
		}

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
					->select(DB::raw("
						service_requests.id AS `history`, requested_procedures.study_instance_id AS `request`, patient_types.description AS `patientType`,
						requested_procedures.id AS `orderNumber`, procedures.description AS `procedureName`, 
						". $admisionDate ." AS `procedureAdmision`, requested_procedures.dictation_date AS `procedureDictation`,
						requested_procedures.approval_date AS `procedureApproval`, procedures." . $fee . " AS `cost`,
						" . $userId . " AS `userId`"))
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join('service_requests','service_requests.id','=','requested_procedures.service_request_id')
					->join('patient_types','patient_types.id','=','service_requests.patient_type_id')
					->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
					//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                    ->whereIn('modalities.id',$data['data']['modalities'])
					->whereIn('service_requests.patient_type_id',$data['data']['patientType'])
					->whereIn($userId,$data['data']['users'])
					->whereIn('requested_procedures.requested_procedure_status_id', $status)
					->whereBetween($admisionDate,[$date1, $date2])
					->where([ 
						[$admisionDate,'!=','0000-00-00 00:00:00']
					])
					->orderBy(DB::raw("`history`"))
					->get();

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function reportAppointment($data){

		$colums = array("date","procedureName","status","room","patientName","patientId","observations","grants","grantsDate",
            "responsible","responsibleDate","previousDate");
		$types = array("date VARCHAR(50)","procedureName VARCHAR(50)","status VARCHAR(50)","room VARCHAR(50)",
            "patientName VARCHAR(50)","patientId VARCHAR(50)","observations VARCHAR(150)","grants VARCHAR(50)",
            "grantsDate VARCHAR(50)","responsible VARCHAR(50)","responsibleDate VARCHAR(50)","previousDate VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('appointments')
					->select(DB::raw("
						appointment_date_start AS `date`, procedures.description AS `procedureName`, 
						appointment_statuses.description AS `status`, rooms.name AS `room`,
						CONCAT(appointments.patient_first_name,' ',appointments.patient_last_name) AS `patientName`,
				        appointments.patient_identification_id AS `patientId`, appointments.observations AS `observations`,
				        appointments.user_id AS `grants`, appointments.created_at AS `grantsDate`, 
				        appointments.applicant AS `responsible`, appointments.updated_at AS `responsibleDate`,
				        appointments.previous_appointment_date_start AS `previous_date`"))
					->join('rooms','rooms.id','=','appointments.room_id')
					->join('procedures','procedures.id','=','appointments.procedure_id')
					//->join('modalities','modalities.id','=','appointments.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join('appointment_statuses','appointment_statuses.id','=','appointments.appointment_status_id')
					->whereIn('appointments.room_id',$data['data']['rooms'])
					->whereIn('appointments.procedure_id',$data['data']['procedure'])
					//->whereIn('appointments.modality_id',$data['data']['modalities'])     //NO
                    ->whereIn('modalities.id',$data['data']['modalities'])
					->whereIn('appointments.appointment_status_id',$data['data']['appointmentStatus'])
					->whereBetween('appointment_date_start',[$date1, $date2])
					->get();

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function reportOrderFor($data){
		
		$this->getDataArray($data,"patientType");

		$colums = array("orders","procedureName","procedureModality","patientId","patientName","realizedDate","time","patientType");
		$types = array("orders INT","procedureName VARCHAR(50)","procedureModality VARCHAR(50)","patientId VARCHAR(50)","patientName VARCHAR(50)","realizedDate VARCHAR(50)","time VARCHAR(50)","patientType VARCHAR(50)");
		
		$previousDate = "";
		$nextDate = "";
		$status = "";

		if($data['data']['type'] == 'Aprobar'){
			//$previousDate = 'service.created_at';
			//$nextDate = 'requested_procedures.technician_end_date';
			$previousDate = 'requested_procedures.transcription_date';
			$nextDate = 'requested_procedures.approval_date';
			$status = [5];	// Orden con status to_approve
		}elseif($data['data']['type'] == 'Transcribir'){
			$previousDate = 'requested_procedures.dictation_date';
			$nextDate = 'requested_procedures.transcription_date';
			$status = [4];	// Orden con status to_transcriber
		}elseif($data['data']['type'] == 'Dictar'){
			$previousDate = 'requested_procedures.technician_end_date';
			$nextDate = 'requested_procedures.dictation_date';
			$status = [3];	// Orden con status to_dictate
		}

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
                ->select(DB::raw("
						requested_procedures.id AS `orders`, procedures.description AS `procedureName`, 
						modalities.name AS `procedureModality`, `patientId`, `patientName`,
						" . $previousDate . " AS `realizedDate`, 
						CONCAT(ROUND((TIMESTAMPDIFF(MINUTE," . $previousDate . ", NOW())/60),2),' ','Horas') AS `time`, `patientType`"))
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join(	DB::raw("( SELECT DISTINCT service_requests.id, service_requests.patient_identification_id AS `patientId`,
						patient_types.description AS `patientType`, CONCAT(service_requests.patient_first_name,' ',service_requests.patient_last_name) AS `patientName`,
				        service_requests.created_at
						FROM service_requests
				        INNER JOIN patient_types
				        ON patient_types.id = service_requests.patient_type_id
						WHERE service_requests.patient_type_id IN ( ". $this->patientType ." )  ) AS service"), function($join){
							$join->on('service.id','=','requested_procedures.service_request_id');
					})
					->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
					//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                    ->whereIn('modalities.id',$data['data']['modalities'])
					->where([ 
						[$previousDate,'!=','0000-00-00 00:00:00'],
						[$nextDate,'=','0000-00-00 00:00:00'],
					])
					->whereIn('requested_procedures.requested_procedure_status_id', $status)
					->whereBetween($previousDate,[$date1, $date2])
					->orderBy(DB::raw("`patientType`"))
					->get();	

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function reportOrders($data){

		$this->getDataArray($data,"patientType");
		$this->getDataArray($data,"users");

		$colums = array("startDate","endDate","patientId","procedureModality","orders","procedureName","time","userId");
		$types = array("startDate VARCHAR(50)","endDate VARCHAR(50)","patientId VARCHAR(50)","procedureModality VARCHAR(50)","orders INT","procedureName VARCHAR(50)","time DOUBLE","userId VARCHAR(50)");
		
		$previousDate = "";
		$nextDate = "";
		$status = "";

		if($data['data']['type'] == 'Realizadas'){
            $user = 'requested_procedures.technician_user_id';
			$previousDate = 'service.created_at';
			$nextDate = 'requested_procedures.technician_end_date';
			$status = [3,4,5,6];	// Orden con status to-dictate en adelante
		}elseif($data['data']['type'] == 'Dictadas'){
            $user ='requested_procedures.radiologist_user_id';
			$previousDate = 'requested_procedures.technician_end_date';
			$nextDate = 'requested_procedures.dictation_date';
			$status = [4,5,6];	// Orden con status to-transcriber en adelante
		}elseif($data['data']['type'] == 'Transcritas'){
            $user ='requested_procedures.transcriptor_user_id';
			$previousDate = 'requested_procedures.dictation_date';
			$nextDate = 'requested_procedures.transcription_date';
			$status = [5,6];	// Orden con status to-approve en adelante
		}elseif($data['data']['type'] == 'Aprobadas'){
            $user ='requested_procedures.approve_user_id';
			// se coloca dictation_date en vez de transcription_date debido a que en la pantalla de ordenes por dictar existen dos botones "terminar orden" lo que lleva en el flujo a "transcripcion", y hay otro boton "terminar y aprobar orden" lo que lleva en el flujo a "aprobar" sin pasar por transcripcion, es decir no siempre el campo transcription_date tendra valor cuando el status es 6
			$previousDate = 'requested_procedures.dictation_date'; 
			$nextDate = 'requested_procedures.approval_date';
			$status = [6];	// Orden con status finish
		}

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
					->select(DB::raw("
						" . $previousDate . " AS `startDate`, " . $nextDate . " AS `endDate`,
						`patientId`, modalities.name AS `procedureModality`, requested_procedures.id AS `orders`,
						procedures.description AS `procedureName`,
						CONCAT(ROUND((TIMESTAMPDIFF(MINUTE," . $previousDate . ", " . $nextDate . ")/60),2),' ','Horas') AS `time`, $user AS `userId`"))
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join(	DB::raw("( SELECT DISTINCT service_requests.id, service_requests.patient_identification_id AS `patientId`,
						patient_types.description AS `patientType`, service_requests.created_at, service_requests.user_id,
						CONCAT(service_requests.patient_first_name,' ',service_requests.patient_last_name) AS `patientName`
						FROM service_requests
						INNER JOIN patient_types
				        ON patient_types.id = service_requests.patient_type_id
						WHERE service_requests.patient_type_id IN ( ". $this->patientType ." ) ) AS service"), function($join){
							$join->on('service.id','=','requested_procedures.service_request_id');
						})
					->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
					//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                    ->whereIn('modalities.id',$data['data']['modalities'])
					->where([ 
						[$previousDate,'!=','0000-00-00 00:00:00'],
						[$nextDate,'!=','0000-00-00 00:00:00'],
					])
					->whereIn('requested_procedures.requested_procedure_status_id', $status)
                    ->whereIn($user, $data['data']['users'])
					->whereBetween($nextDate,[$date1, $date2])
					->orderBy(DB::raw("`userId`"))
					->get();
                    //->toSql();

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	/** 
     * @fecha 17-06-2017
     * @programador Joan Charaima  
     * @objetivo Genera el repote por genero con los filtros de fecha, tipo paciente, procedimiento y modalidad
     * @ param $data
     */
	private function reportGender($data){

		// se almacenan en las variables cadenas patientType, procedure y modalities lo recuperado desde $data
		$this->getDataArray($data,"patientType");
		$this->getDataArray($data,"procedure");
		$this->getDataArray($data,"modalities");

		$colums = array("gender","countGender");
		$types = array("gender VARCHAR(50)","countGender VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('service_requests')
					// selecciona los campos iniciales de agrupacion de la tabla
					->select(DB::raw("service_requests.patient_sex_id AS `gender`, COUNT(service_requests.patient_sex_id) AS `countGender`"))
					// Filtro para escoger los ultimos id ( MAX(id) )
					->join(	DB::raw("( SELECT MAX(id) AS `id`, patient_id
						FROM service_requests
						GROUP BY patient_id) AS filters"), function($join){
							$join->on('filters.id','=','service_requests.id');
						})
					// Filtro de tipo paciente
					->join(	DB::raw("( SELECT id FROM patient_types WHERE id IN ( " . $this->patientType . " ) ) AS patients"), function($join){
							$join->on('patients.id','=','service_requests.patient_type_id');
						})
					// Se agrupa por el sexo del paciente
					->groupBy("service_requests.patient_sex_id")
					->orderBy(DB::raw("service_requests.patient_sex_id"))
					// Filtro donde se evalua el procedimiento contra la tabla requested procedure
                    ->join(	DB::raw(" (SELECT service_request_id, procedure_id FROM requested_procedures WHERE procedure_id IN ( " . $this->procedure .
                        " ) GROUP BY service_request_id) As requested"), function($join){
                        $join->on('requested.service_request_id','=','service_requests.id');
                    })
                    /* Filtro donde se evalua la modalidad contra la tabla procedures, para ello se utiliza el campo
                    requested.procedure_i el cual se obtiene en la consulta del join anterior */
                    ->join(	DB::raw(" (SELECT id FROM procedures WHERE modality_id IN ( " . $this->modalities .
                        " ) GROUP BY id) As proced"), function($join){
                        $join->on('proced.id','=','requested.procedure_id');
                    })
					->whereBetween('issue_date',[$date1, $date2])
					->get();

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function reportEfficient($data){

		$colums = array("patientType","orderId","procedureName","procedureModality","patientId","patientName","accomplishedDate","dictationDate","transcriptionDate","approvalDate","userId","t1","t2","total");
		$types = array("patientType VARCHAR(50)","orderId VARCHAR(50)","procedureName VARCHAR(50)","procedureModality VARCHAR(50)","patientId VARCHAR(50)","patientName VARCHAR(50)","accomplishedDate VARCHAR(50)","dictationDate VARCHAR(50)","transcriptionDate VARCHAR(50)","approvalDate VARCHAR(50)","userId VARCHAR(50)","t1 VARCHAR(50)","t2 VARCHAR(50)","total VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
					->select(DB::raw("
						patient_types.description AS `patientType`, requested_procedures.id AS `orderId`, 
						procedures.description AS `procedureName`, modalities.name AS `procedureModality`, 
						service_requests.patient_identification_id AS `patientId`, 
						CONCAT(service_requests.patient_first_name,' ',service_requests.patient_last_name) AS `patientName`,
						requested_procedures.technician_end_date AS `accomplishedDate`, requested_procedures.dictation_date AS `dictationDate`, 
						requested_procedures.transcription_date AS `transcriptionDate`, requested_procedures.approval_date AS `approvalDate`, 
						service_requests.user_id AS `userId`,
						CONCAT(ROUND((TIMESTAMPDIFF(MINUTE,requested_procedures.technician_end_date, requested_procedures.approval_date)/60),2),' ','Horas') AS `t1`,
						0 AS `t2`, 
						( CONCAT(ROUND((TIMESTAMPDIFF(MINUTE,requested_procedures.technician_end_date, requested_procedures.approval_date)/60),2),' ','Horas')+0 ) AS `total`"))
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
                    /*->join(	DB::raw("( SELECT name
						FROM modalities) AS modality"), function($join){
                        $join->on('modality.id','=','procedures.modality_id');
                    })*/
					->join('requested_procedure_statuses','requested_procedure_statuses.id','=','requested_procedures.requested_procedure_status_id')
					->join('service_requests','service_requests.id','=','requested_procedures.service_request_id')
					->join('patient_types','patient_types.id','=','service_requests.patient_type_id')
					->where([ 
						['requested_procedures.technician_end_date','!=','0000-00-00 00:00:00'],
						['requested_procedures.dictation_date','!=','0000-00-00 00:00:00'],
						['requested_procedures.transcription_date','!=','0000-00-00 00:00:00'],
						['requested_procedures.approval_date','!=','0000-00-00 00:00:00'],
						['requested_procedure_statuses.description','=','finished']
					])
					->whereBetween('requested_procedures.technician_end_date',[$date1, $date2])
					->orderBy(DB::raw("service_requests.user_id"))
					->get();	

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function countOrderHonorary($data){

		$this->getDataArray($data,"users");

		//$colums = array("userName","orderNumber","amount","countAmount","countOrder");
		//$types = array("userName VARCHAR(50)","orderNumber INT","amount VARCHAR(50)","countAmount VARCHAR(50)","countOrder VARCHAR(50)");

		$colums = array("userName","orderNumber","amount","perOrder","perAmount",);
		$types = array("userName VARCHAR(50)","orderNumber INT","amount VARCHAR(50)","perOrder VARCHAR(50)","perAmount VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$userId = "";
		$admisionDate = "";
		$fee = "";
		$status = "";

		if($data['data']['type'] == 'Radiologo'){
			$userId = 'requested_procedures.radiologist_user_id';
			$admisionDate = 'requested_procedures.approval_date';
			$fee = 'radiologist_fee';
			$status = [6];	// Orden con status Finish
		}elseif($data['data']['type'] == 'Transcriptor'){
			$userId = 'requested_procedures.transcriptor_user_id';
			$admisionDate = 'requested_procedures.transcription_date';
			$fee = 'transcriptor_fee';
			$status = [5, 6];	// Orden con status to_approve, finish
		}elseif($data['data']['type'] == 'Tecnico'){
			$userId = 'requested_procedures.technician_user_id';
			$admisionDate = 'requested_procedures.technician_end_date';
			$fee = 'technician_fee';
			$status = [3, 4, 5, 6];	// Orden con status to_dictate, to_transcriber, to_approve, finish
		}

		$result = DB::table('requested_procedures')
				->select(DB::raw(
					$userId . " AS `userId`, COUNT(" . $userId . ") AS `counts`, SUM( procedures." . $fee . ") AS `amounts`"))
				->join('procedures','procedures.id','=','requested_procedures.procedure_id')
				//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
				->join('service_requests','service_requests.id','=','requested_procedures.service_request_id')
				->join('patient_types','patient_types.id','=','service_requests.patient_type_id')
				->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
				//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                ->whereIn('modalities.id',$data['data']['modalities'])
				->whereIn('service_requests.patient_type_id',$data['data']['patientType'])
				->whereIn($userId,$data['data']['users'])
				->whereIn('requested_procedures.requested_procedure_status_id', $status)
				->whereBetween($admisionDate,[$date1, $date2])
				->where([ 
					[$admisionDate,'!=','0000-00-00 00:00:00']
				])
				->groupBy($userId)
				->get();
	
		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function countOrderAppointment($data){

		$colums = array("status","counts","countsStatus");
		$types = array("status VARCHAR(50)","counts VARCHAR(50)","countsStatus VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('appointments')
					->select(DB::raw("
						appointment_statuses.description AS `status`, COUNT(appointment_statuses.id) AS `counts`"))
					->join('rooms','rooms.id','=','appointments.room_id')
					->join('procedures','procedures.id','=','appointments.procedure_id')
					//->join('modalities','modalities.id','=','appointments.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join('appointment_statuses','appointment_statuses.id','=','appointments.appointment_status_id')
					->whereIn('appointments.room_id',$data['data']['rooms'])
					->whereIn('appointments.procedure_id',$data['data']['procedure'])
					//->whereIn('appointments.modality_id',$data['data']['modalities'])
                    ->whereIn('modalities.id',$data['data']['modalities'])
					->whereIn('appointments.appointment_status_id',$data['data']['appointmentStatus'])
					->whereBetween('appointment_date_start',[$date1, $date2])
					->groupBy(DB::raw("appointment_statuses.description"))
					->get();

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	/** MODIFICACION
     * @fecha 18-06-2017
     * @programador Joan Charaima  
     * @objetivo Ejecuta el query necesario para generar la tabla secondTemporal para los reportes. La modificacion basicamente se realizo debido a que los valores porcentuales mostrados en la aplicacion estaban errados.
     * @param $data
     */
	private function countOrdersFor($data){
		
		$this->getDataArray($data,"patientType");

		$colums = array("patientType","orders","countsOrder");
		$types = array("patientType VARCHAR(50)","orders INT","countsOrder VARCHAR(50)");

		$previousDate = "";
		$nextDate = "";
		$status = "";

		if($data['data']['type'] == 'Aprobar'){
			//$previousDate = 'service.created_at';
			//$nextDate = 'requested_procedures.technician_end_date';
			$previousDate = 'requested_procedures.transcription_date';
			$nextDate = 'requested_procedures.approval_date';
			$status = [5];	// Orden con status to_approve
		}elseif($data['data']['type'] == 'Transcribir'){
			$previousDate = 'requested_procedures.dictation_date';
			$nextDate = 'requested_procedures.transcription_date';
			$status = [4];	// Orden con status to_transcriber
		}elseif($data['data']['type'] == 'Dictar'){
			$previousDate = 'requested_procedures.technician_end_date';
			$nextDate = 'requested_procedures.dictation_date';
			$status = [3];	// Orden con status to_dictate
		}

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
					->select(DB::raw("`patientType`, COUNT(`patientTypeId`) AS `counts`"))
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join(	DB::raw("( SELECT DISTINCT service_requests.id, patient_types.description AS `patientType`,
						patient_types.id AS `patientTypeId`, service_requests.created_at
						FROM service_requests
				        INNER JOIN patient_types
				        ON patient_types.id = service_requests.patient_type_id
						WHERE service_requests.patient_type_id IN ( ". $this->patientType ." )  ) AS service"), function($join){
							$join->on('service.id','=','requested_procedures.service_request_id');
						})
					->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
					//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                    ->whereIn('modalities.id',$data['data']['modalities'])
					->where([ 
						[$previousDate,'!=','0000-00-00 00:00:00'],
						[$nextDate,'=','0000-00-00 00:00:00'],
					])
					->whereIn('requested_procedures.requested_procedure_status_id', $status)
					->whereBetween($previousDate,[$date1, $date2])
					->groupBy(DB::raw("`patientType`"))
					->get();	

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

	private function countOrders($data){
			
			$this->getDataArray($data,"patientType");
			$this->getDataArray($data,"users");

			$colums = array("userName","orderNumber","countOrder");
			$types = array("userName VARCHAR(50)","orderNumber VARCHAR(50)","countOrder VARCHAR(50)");
			
			$previousDate = "";
			$nextDate = "";
			$status = "";

			if($data['data']['type'] == 'Realizadas'){
                $user = 'requested_procedures.technician_user_id';
				$previousDate = 'service.created_at';
				$nextDate = 'requested_procedures.technician_end_date';
				$status = [3,4,5,6];	// Orden con status to-dictate en adelante
			}elseif($data['data']['type'] == 'Dictadas'){
                $user ='requested_procedures.radiologist_user_id';
				$previousDate = 'requested_procedures.technician_end_date';
				$nextDate = 'requested_procedures.dictation_date';
				$status = [4,5,6];	// Orden con status to-transcriber en adelante
			}elseif($data['data']['type'] == 'Transcritas'){
                $user ='requested_procedures.transcriptor_user_id';
				$previousDate = 'requested_procedures.dictation_date';
				$nextDate = 'requested_procedures.transcription_date';
				$status = [5,6];	// Orden con status to-transcriber en adelante
			}elseif($data['data']['type'] == 'Aprobadas'){
                $user ='requested_procedures.approve_user_id';
				// se coloca dictation_date en vez de transcription_date debido a que en la pantalla de ordenes por dictar existen dos botones "terminar orden" lo que lleva en el flujo a "transcripcion", y hay otro boton "terminar y aprobar orden" lo que lleva en el flujo a "aprobar" sin pasar por transcripcion, es decir no siempre el campo transcription_date tendra valor cuando el status es 6
				$previousDate = 'requested_procedures.dictation_date'; 
				$nextDate = 'requested_procedures.approval_date';
				$status = [6];	// Orden con status finish
			}

			// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
			$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
			$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

			$result = DB::table('requested_procedures')
						->select(DB::raw("
							$user AS `userId`, COUNT(service.user_id) AS `counts`"))
						->join('procedures','procedures.id','=','requested_procedures.procedure_id')
						//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                        ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
						->join(	DB::raw("( SELECT DISTINCT service_requests.id, service_requests.user_id, service_requests.created_at
							FROM service_requests
							INNER JOIN patient_types
					        ON patient_types.id = service_requests.patient_type_id
							WHERE service_requests.patient_type_id IN ( ". $this->patientType ." ) ) AS service"), function($join){
								$join->on('service.id','=','requested_procedures.service_request_id');
							})
						->whereIn('requested_procedures.procedure_id',$data['data']['procedure'])
						//->whereIn('requested_procedures.modality_id',$data['data']['modalities'])
                        ->whereIn('modalities.id',$data['data']['modalities'])
						->where([ 
							[$previousDate,'!=','0000-00-00 00:00:00'],
							[$nextDate,'!=','0000-00-00 00:00:00'],
						])
						->whereIn('requested_procedures.requested_procedure_status_id', $status)
                        ->whereIn($user, $data['data']['users'])
						->whereBetween($nextDate,[$date1, $date2])
						->groupBy(DB::raw($user))
						->get();

			return ['colums' => $colums,
					'types' => $types,
					'total' => count($result),
					'results' => $result];
	}

	private function countEfficient($data){
		
		$colums = array("userId","orderNumber","countOrder");
		$types = array("userId VARCHAR(50)","orderNumber VARCHAR(50)","countOrder VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

		$result = DB::table('requested_procedures')
					->select(DB::raw("service_requests.user_id AS `userId`, COUNT(service_requests.user_id) AS `counts`"))
					->join('procedures','procedures.id','=','requested_procedures.procedure_id')
					//->join('modalities','modalities.id','=','requested_procedures.modality_id'
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->join('requested_procedure_statuses','requested_procedure_statuses.id','=','requested_procedures.requested_procedure_status_id')
					->join('service_requests','service_requests.id','=','requested_procedures.service_request_id')
					->join('patient_types','patient_types.id','=','service_requests.patient_type_id')
					->where([ 
						['requested_procedures.technician_end_date','!=','0000-00-00 00:00:00'],
						['requested_procedures.dictation_date','!=','0000-00-00 00:00:00'],
						['requested_procedures.transcription_date','!=','0000-00-00 00:00:00'],
						['requested_procedures.approval_date','!=','0000-00-00 00:00:00'],
						['requested_procedure_statuses.description','=','finished']
					])
					->whereBetween('requested_procedures.technician_end_date',[$date1, $date2])
					->groupBy(DB::raw("service_requests.user_id"))
					->orderBy(DB::raw("service_requests.user_id"))
					->get();	

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

/** 
     * @fecha 27-06-2017
     * @programador Joan Charaima  
     * @objetivo Funcion que realiza la generacion del reporte de "Transcripcion"
     * @param $data
     */
	private function reportTranscription($data) {

		$colums = array("userId", "dateNumber", "transcriptionDate", "CR","CT", "MG", "MR", "NM", "OT", "RF", "US", "XA", "XR");
		$types = array("userId VARCHAR(50)", "dateNumber VARCHAR(50)", "transcriptionDate VARCHAR(50)", "CR VARCHAR(50)","CT VARCHAR(50)", "MG VARCHAR(50)", "MR VARCHAR(50)", "NM VARCHAR(50)", "OT VARCHAR(50)", "RF VARCHAR(50)", "US VARCHAR(50)", "XA VARCHAR(50)", "XR VARCHAR(50)");

		// se obtiene los parametros de la fecha inicio y fecha fin y se le concatena las horas
		$date1 = new DateTime($data['data']['startDate'] .  ' 00:00:00');
		$date2 = new DateTime($data['data']['endDate'] .  ' 23:59:59');

        //dump($data['data']['users'][0]);

		$result = DB::table('requested_procedures')
					->select(DB::raw("requested_procedures.transcriptor_user_id as userId, CONCAT(date_format(transcription_date,'%Y%m'),'01') as transcriptionDate1, date_format(transcription_date,'%M %Y') AS transcriptionDate2, modalities.name, count(procedures.modality_id) AS `counts`"))
                    //->select(DB::raw("requested_procedures.transcriptor_user_id as userId, CONCAT(date_format(transcription_date,'%Y%m'),'01') as transcriptionDate1, date_format(transcription_date,'%M %Y') AS transcriptionDate2, modalities.name, count(requested_procedures.*) AS `counts`"))
					//->join('modalities','modalities.id','=','requested_procedures.modality_id')
                    ->join('procedures','procedures.id','=','requested_procedures.procedure_id')
                    ->join('modalities','modalities.id','=','procedures.modality_id')   // REVISAR
					->whereIn('requested_procedures.transcriptor_user_id', $data['data']['users'])
					->whereBetween('requested_procedures.transcription_date',[$date1, $date2])
					->groupBy(DB::raw("requested_procedures.transcriptor_user_id, transcriptionDate1, transcriptionDate2, modalities.name"))
					->orderBy(DB::raw("requested_procedures.transcriptor_user_id, transcriptionDate1, modalities.name"))
					->get();

		$modality = DB::table('modalities')->select('modalities.name as name')->distinct()
					->orderBy(DB::raw("modalities.name"))
					->get();

		if (isset($result)) {

			/* Se realiza el recorrido tanto del resultado obtenido desde la BD como de las modalidades con el fin de realizar las comparaciones e ir armando la estructura requeridad por el reporte de transcripciones */
			$resultsAux = array();
	        $userAnt = 0;
	        $dateAnt ="";
	        $i = -1;

	        // se recorre cada uno de los resultados obtenidos desde la BD
			foreach( $result as $results ) {
				// si el usuario es diferente al usuario anterior OR la fecha es diferente a la fecha anterior
	        	if ( ($results->userId != $userAnt) || ($results->transcriptionDate2 != $dateAnt) ) {       
	                $i++;
	                // se agregar el nuevo usuario
	                $resultAux[$i] = ['userId' => $results->userId];
	                // se iguala al usuario anterior, el usuario actual
	                $userAnt = $results->userId;
	                
	                // se agregar la nueva fecha
	                $resultAux[$i]['dateNumber'] = $results->transcriptionDate1;
	                $resultAux[$i]['transcriptionDate'] = $results->transcriptionDate2;
	                // se iguala a la fecha anterior, la usuario actual
	                $dateAnt = $results->transcriptionDate2;
	            }

	            // se recorre cada una de las modalidades almacendas en la BD
	            foreach($modality as $modalities )
	            	// si el nombre de la modalidad del resultado es igual al nomrbre de la modalidad
	                if ($results->name == $modalities->name) 
	                	// se agregar al arreglo en la posicion i y la subposicion "nombre de la modalidad" la cantidad obtenida de la BD
	                    $resultAux[$i][$modalities->name] = $results->counts;
	                // sino existe en el resultado ninguna modalidad con ese nombre, se asigna 0 a la cantidad
	                elseif (!isset($resultAux[$i][$modalities->name]))
	                        $resultAux[$i][$modalities->name] = 0;
	       	}
   		} 

   		if (isset($resultAux))
   			$result = $resultAux;

		return ['colums' => $colums,
				'types' => $types,
				'total' => count($result),
				'results' => $result];
	}

}	// Fin de la clase