<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SmokingStatus;
use Activity;
use Log;

class SmokingStatusesController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $smokingStatuses = SmokingStatus::orderBy('id', 'asc')->get();
            return $smokingStatuses;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: smokingStatuses. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
