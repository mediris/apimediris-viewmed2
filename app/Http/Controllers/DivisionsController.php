<?php

namespace App\Http\Controllers;

use App\Division;
use Activity;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;

class DivisionsController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Divisions.
     */
    public function index()
    {
        try
        {
            $divisions = Division::orderBy('name', 'asc')->get();

            return $divisions;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: divisions. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Division.
     */
    public function show(Division $division, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'division', 'id' => $division->id]), $request->all()['user_id']);

        return $division;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Agregar una nueva instancia de Division.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|max:50',
            ]);

            $division = new Division($request->all());

            try
            {
                if($division->save())
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'division', 'id' => $division->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.division')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'division', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.division')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: divisions. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $division->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de Division.
     */
    public function edit(Request $request, Division $division)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|max:50'
            ]);

            $original = new Division();
            foreach($division->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $division->active = 0;

            try
            {
                if($division->update($request->all()))
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'division', 'id' => $division->id, 'oldValue' => $original, 'newValue' => $division]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.division')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $division->id, 'section' => 'division', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.division')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: divisions. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $division]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Eliminar una instancia de Division.
     */
    public function delete(Request $request, Division $division)
    {
        try
        {
            if($division->delete())
            {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $division->id, 'section' => 'division']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.division')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $division->id, 'section' => 'division', 'action' => 'delete']), $request->all()['user_id']);
                
                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.division')]));
                $request->session()->flash('class', 'alert alert-danger');
            }

            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: divisions. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado de activo de una instancia de Division.
     */
    public function active(Request $request, Division $division)
    {
        try
        {

            $original = new Division();
            foreach($division->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $division->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'divisions', 'id' => $division->id, 'oldValue' => $original, 'newValue' => $division, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: divisions. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $division]);
    }
}
