<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PrinterRequest;
use App\Printer;
use App\Http\Requests;
use Activity;
use Log;

class PrintersController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Printers.
     */
    public function index()
    {
        try
        {
            $printers = Printer::all();
            return $printers;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: printers. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Printer.
     */
    public function show(Printer $printer, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'printer', 'id' => $printer->id]), $request->all()['user_id']);

        return $printer;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Agregar una nueva instancia de Printer.
     */
    public function add(PrinterRequest $request)
    {
        if($request->isMethod('post'))
        {
            $printer = new Printer($request->all());

            try
            {
                if($printer->save())
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'printer', 'id' => $printer->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.printer')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'printer', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.printer')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: printer. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $printer->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de Printer.
     */
    public function edit(PrinterRequest $request, Printer $printer)
    {
        if($request->isMethod('post'))
        {

            $original = new Printer();
            foreach($printer->getOriginal() as $key => $value)
            {
                $printer->$key = $value;
            }

            $printer->active = 0;

            try
            {
                if($printer->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'printer', 'id' => $printer->id, 'oldValue' => $original, 'newValue' => $printer]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.printer')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $printer->id, 'section' => 'printer', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.printer')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: printers. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $printer]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Borrar una instancia de Printer. (NO ESTÁ EN USO)
     */
    public function delete(PrinterRequest $request, Printer $printer)
    {
        try
        {
            if($printer->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $printer->id, 'section' => 'printer']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.printer')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $printer->id, 'section' => 'printer', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.printer')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: printers. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado active de una instancia de Printer.
     */
    public function active(Request $request, Printer $printer)
    {
        try
        {
            $original = new Printer();
            foreach($printer->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $printer->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'printers', 'id' => $printer->id, 'oldValue' => $original, 'newValue' => $printer, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: printers. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $printer]);
    }
}
