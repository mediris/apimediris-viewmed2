<?php

namespace App\Http\Controllers;

use App\RequestedProcedure;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Equipment;
use App\Procedure;
use Activity;
use Log;

class EquipmentController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Equipment.
     */
    public function index(Request $request)
    {
        try
        {   
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $equipment = Equipment::where($where)->get();
                
            } else {
                $equipment = Equipment::all();
            }
            

            return $equipment;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: equipment. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Agregar una nuena instancia de Equipment.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'ae_title' => 'required|max:50',
                'name' => 'required|max:50',
            ]);

            $equipment = new Equipment($request->all());

            try
            {
                if($equipment->save())
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'equipment', 'id' => $equipment->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.equipment')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'equipment', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.equipment')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: equipment. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $equipment->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Equipment.
     */
    public function show(Equipment $equipment, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'equipment', 'id' => $equipment->id]), $request->all()['user_id']);


        return $equipment;
    }

    public function showbyprocedure(Procedure $procedure) {
        //return $procedure->getEquipments();
        return RequestedProcedure::getEquipment($procedure->id);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de Equipment.
     */
    public function edit(Request $request, Equipment $equipment)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'ae_title' => 'required|max:50',
                'name' => 'required|max:50',
            ]);

            $original = new Equipment();
            foreach($equipment->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }


            $equipment->active = 0;

            try
            {
                if($equipment->update($request->all()))
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'equipment', 'id' => $equipment->id, 'oldValue' => $original, 'newValue' => $equipment]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.equipment')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $equipment->id, 'section' => 'equipment', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.equipment')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: equipment. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $equipment]);
        }

    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Eliminar una instancia de Equipment.
     */
    public function delete(Request $request, Equipment $equipment)
    {
        try
        {
            if($equipment->delete())
            {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $equipment->id, 'section' => 'equipment']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.equipment')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $equipment->id, 'section' => 'equipment', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.equipment')]));
                $request->session()->flash('class', 'alert alert-danger');
            }

            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: equipment. Action: delete');
            
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado de active de una instancia de Equipment.
     */
    public function active(Request $request, Equipment $equipment)
    {
        try
        {

            $original = new Equipment();
            foreach($equipment->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $equipment->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'equipment', 'id' => $equipment->id, 'oldValue' => $original, 'newValue' => $equipment, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: equipment. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $equipment]);
    }
}
