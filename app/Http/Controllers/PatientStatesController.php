<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientState;
use App\Http\Requests;
use Activity;
use Log;

class PatientStatesController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de PatientStates.
     */
    public function index()
    {
        try
        {
            $patientStates = PatientState::orderBy('description', 'asc')->get();
            return $patientStates;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientStates. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de PatientState.
     */
    public function show(PatientState $patientState, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'patientStates', 'id' => $patientState->id]), $request->all()['user_id']);


        return $patientState;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Crear una nueva instancia de PatientState.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'language' => 'required',
            ]);

            $patientState = new PatientState($request->all());


            try
            {

                $patientState->save();

                /**
                 * Log activity
                 */

                    Activity::log(trans('tracking.create', ['section' => 'patientStates', 'id' => $patientState->id]), $request->all()['user_id']);



            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: add');

                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $patientState->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de PatientState.
     */
    public function edit(Request $request, PatientState $patientState)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'language' => 'required',
            ]);

            $original = new PatientState();
            foreach($patientState->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $patientState->active();


            try
            {

                $patientState->update($request->all());

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit', ['section' => 'patientType', 'id' => $patientState->id, 'oldValue' => $original, 'newValue' => $patientState]), $request->all()['user_id']);



            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientStates. Action: edit');

                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $patientState]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado active de una instancia de PatientState.
     */
    public function active(Request $request, PatientState $patientState)
    {
        try
        {

            $original = new PatientState();
            foreach($patientState->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $patientState->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'patientStates', 'id' => $patientState->id, 'oldValue' => $original, 'newValue' => $patientState, 'action' => 'active']), $request->all()['user_id']);
            

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientStates. Action: active');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $patientState]);
    }
}
