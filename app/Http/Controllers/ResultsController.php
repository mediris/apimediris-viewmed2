<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RequestedProcedure;
use Activity;
use Log;
use DataSourceResult;

class ResultsController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $host = config('database.connections.mysql.host');
            $dbname = config('database.connections.mysql.database');
            $conn = config('database.default');
            $username = config('database.connections.mysql.username');
            $password = config('database.connections.mysql.password');

            $DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);

            $req = $request->all();

            $data = (object) $req['data'];

            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }

            if(isset($data->sort))
            {
                foreach ($data->sort as $key => $sortArray)
                {
                    $data->sort[$key] = (object) $sortArray;
                }
            }

            $fields = ['isUrgent','patientTypeIcon','patientType','approvalDate','serviceRequestID','orderID','procedureDescription','patientName'];
            $fields['patientID'] = ['type' => 'string'];    //This field needs a data type because with some values (14717687-2) it gets converted to a date  
            $result = $DbHandler->read('resultsIndexView', $fields, $data);

            return response()->json(['code' => '200', 'data' => $result]);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/results/results.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Results. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    public function show(Request $request, RequestedProcedure $requestedProcedure)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'results', 'id' => $requestedProcedure->id]), $request->all()['user_id']);

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize', 'delivers']);

        $requestedProcedure->load(['addendums' => function ($query) use ($requestedProcedure){
            $query->where('requested_procedure_id', $requestedProcedure->id)
                     ->orderBy('id', 'desc')
                     ->limit(1);
        }]);

        $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);

        $requestedProcedure->procedure->load(['templates']);

        return $requestedProcedure;
    }

    public function edit(Request $request, RequestedProcedure $requestedProcedure)
    {
        try
        {

            $original = new RequestedProcedure();
            foreach($requestedProcedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $requestedProcedure->update($request->all());

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'results', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'edit']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/results/results.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: results. Action: edit');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
        $original->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);
    }

    public function getPollsData(Request $request){
        try {
            $query = new RequestedProcedure();
            if( isset( $request->data['data_from'] ) && isset( $request->data['data_to'] ) ) {
                $query = $query->whereBetween('culmination_date',[$request->data['data_from'],$request->data['data_to']]);
            }
            elseif(isset( $request->data['data_from'] )){
                $query = $query->whereDate('culmination_date','>=',$request->data['data_from']);
            }
            elseif(isset( $request->data['data_to'] )){
                $query = $query->whereDate('culmination_date','<=',$request->data['data_to']);
            }
            if(isset( $request->data['procedures'] )){
                foreach($request->data['procedures'] as $key => $procedure) {
                    if($key==0){
                        $query = $query->Where("procedure_id",$procedure);
                    }
                    else{
                        $query = $query->orWhere("procedure_id",$procedure);
                    }

                }

            }
            $results = $query->with('serviceRequest')->groupBy('service_request_id')->get();
          return  response()->json(['code' => '200', 'data' =>$results]);
        }
        catch(\Exception $e) {
            Log::useFiles(storage_path().'/logs/results/results.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: results. Action: edit');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }
}
