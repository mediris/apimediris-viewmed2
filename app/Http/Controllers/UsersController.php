<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use Activity;
use Log;
use Illuminate\Http\Request;
use App\Http\Requests;

class UsersController extends Controller
{
    public function index()
    {
        try
        {
            $users = User::all();
            return $users;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: users. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {

            $this->validate($request, [
                'administrative_ID' => 'required|max:45',
                'api_token_2'=> 'required'
            ]);

            $data = [];

            $data['administrative_ID'] = $request->input('administrative_ID');
            $data['api_token'] = $request->input('api_token_2');

            $user = new User($data);

            try
            {

                $user->save();
                
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create', ['section' => 'user', 'id' => $user->id]), $request->all()['user_id']);
                
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: users. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $user->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function show(User $user, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'user', 'id' => $user->id]), $request->all()['user_id']);
        return $user;
    }

    public function edit(Request $request, User $user)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'administrative_ID' => 'required|unique:users,administrative_ID,'.$user->id.'|max:45',
                'api_token'=> 'required|unique:users,api_token,'.$user->id.'|max:60'
            ]);

            $original = new User();
            foreach($user->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $data = $request->all();
            $user->active = 0;

            try
            {
                if($user->update($data))
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'user', 'id' => $user->id, 'oldValue' => $original, 'newValue' => $user]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.user')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $user->id, 'section' => 'user', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.user')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: users. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated']);
        }
        return $user;
    }

    public function delete(Request $request, User $user)
    {
        try
        {
            if($user->delete())
            {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $user->id, 'section' => 'user']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.user')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $user->id, 'section' => 'user', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.user')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: users. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
