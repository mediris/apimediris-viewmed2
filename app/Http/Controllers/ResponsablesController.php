<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Responsable;
use Activity;
use Log;

class ResponsablesController extends Controller
{
    public function index()
    {
        try
        {
            $responsables = Responsable::all();
            return $responsables;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/patients/patients.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: responsables. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
