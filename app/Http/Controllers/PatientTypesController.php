<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientType;
use App\Http\Requests;
use Activity;
use Log;

class PatientTypesController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de PatientTypes.
     */
    public function index( Request $request ) {
        try {
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $patientTypes = PatientType::where($where)->orderBy('description', 'asc')->get();
            } else {
                $patientTypes = PatientType::orderBy('description', 'asc')->get();
            }

            $patientTypes->load('parentPatientType');
            $patientTypes->load('childPatientTypes');
            return $patientTypes;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de PatientTypes con level => 0 (PatientTypes Padres).
     */
    public function roots()
    {
        try
        {
            $roots = PatientType::all()->where('level', 0);
            $roots->load('parentPatientType');
            $roots->load('childPatientTypes');
            return $roots;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: index');

            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Crear una nueva instancia de PatientType.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'administrative_ID' => 'required|max:50',
                'priority' => 'required',
                //'icon' => 'required',
                'parent_id' => 'required',
            ]);

            $patientType = new PatientType($request->all());

            if($patientType->parent_id == 0)
            {
                $patientType->level = 0;
            }
            else
            {
                $patientType->level = PatientType::find($patientType->parent_id)->level + 1;
            }

            try
            {
                if($patientType->save())
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'patientType', 'id' => $patientType->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.patient-type')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'patientTypes', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.patient-type')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: add');

                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $patientType->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de PatientType.
     */
    public function show(PatientType $patientType, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'patientType', 'id' => $patientType->id]), $request->all()['user_id']);

        $patientType->load('parentPatientType');
        $patientType->load('childPatientTypes');

        return $patientType;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de PatientType.
     */
    public function edit(Request $request, PatientType $patientType)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'administrative_ID' => 'required|max:50',
                'priority' => 'required',
                //'icon' => 'required',
            ]);

            $original = new PatientType();
            foreach($patientType->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $patientType->setZeros();

            if($request->all()['parent_id'] == 0)
            {
                $patientType->level = 0;
            }
            else
            {
                $patientType->level = PatientType::find($request->all()['parent_id'])->level + 1;
            }

            try
            {
                if($patientType->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'patientType', 'id' => $patientType->id, 'oldValue' => $original, 'newValue' => $patientType]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.patient-type')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $patientType->id, 'section' => 'patientType', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.patient-type')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: edit');

                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $patientType]);
        }

        return $patientType;
    }

    public function delete(Request $request, PatientType $patientType)
    {
        try
        {
            if($patientType->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $patientType->id, 'section' => 'patientType']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.patient-type')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $patientType->id, 'section' => 'patientType', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.patient-type')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: delete');

            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        return response()->json(['code' => '200', 'message' => 'Deleted']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de PatientType padre de otro PatientType.
     */
    public function parent(PatientType $patientType)
    {
        return $patientType->parentPatientType()->get();
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de PatientTypes hijos de otro PatientType.
     */
    public function childs(PatientType $patientType)
    {
        return $patientType->childPatientTypes()->get();
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado active de un PatientType.
     */
    public function active(Request $request, PatientType $patientType)
    {
        try
        {

            $original = new PatientType();
            foreach($patientType->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $patientType->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'patientTypes', 'id' => $patientType->id, 'oldValue' => $original, 'newValue' => $patientType, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patientTypes. Action: active');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $patientType]);
    }
}
