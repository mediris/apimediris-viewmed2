<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Division;
use Activity;
use App\Http\Requests;
use Log;

class RoomsController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            /*$rooms = Room::orderBy('name', 'asc')->get();
            $rooms->load(['division', 'procedures']);
            return $rooms;*/

            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $rooms = Room::where($where)->orderBy('name', 'asc')->get();
            } else {
                $rooms = Room::orderBy('name', 'asc')->get();
            }
            $rooms->load(['division', 'procedures']);
            return $rooms;

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: rooms. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(Room $room, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'room', 'id' => $room->id]), $request->all()['user_id']);

        $room->load(['division', 'procedures']);

        return $room;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|max:50',
                'description' => 'required|max:250',
                //'administrative_ID' => 'required|unique:rooms|max:45',
                'division_id' => 'required',
            ]);

            $data = $request->all();
            $data['avoided_blocks'] = "";
            unset($data['procedure_id']);
            
            if(isset($data['blocks_locked']))
            {
                foreach($data['blocks_locked'] as $block)
                {
                    $data['avoided_blocks'] = $data['avoided_blocks'].$block." ";
                }

                trim($data['avoided_blocks']);
            }

            
            $room = new Room($data);

            try
            {
                if($room->save())
                {

                    if(isset($data['procedures']))
                    {
                        foreach($data['procedures'] as $procedure) {
                            if ( $procedure['duration'] == 0 ) {
                                $procedure['duration'] = 1;
                            }
                            $room->procedures()->attach($procedure['id'], ['duration' => $procedure['duration']]);
                        }
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'room', 'id' => $room->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.room')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'room', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.room')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: rooms. Action: add');

                return response()->json(['code' => $e->errorInfo[1],'error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $room->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, Room $room)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|max:50',
                'description' => 'required|max:250',
                //'administrative_ID' => 'required|unique:rooms,administrative_id,'.$room->id.'|max:45',
                'administrative_ID' => 'required|max:45',
                'division_id' => 'required',
            ]);

            $room->active = 0;

            $original = new Room();
            foreach($room->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $data = $request->all();
            $data['avoided_blocks'] = "";
            unset($data['procedure_id']);

            if(isset($data['blocks_locked']))
            {
                foreach($data['blocks_locked'] as $block)
                {
                    $data['avoided_blocks'] = $data['avoided_blocks'].$block." ";
                }

                trim($data['avoided_blocks']);
            }

            try
            {
                if($room->update($data))
                {
                    $room->procedures()->detach();
                    
                    if(isset($data['procedures']))
                    {
                        foreach($data['procedures'] as $procedure) {
                            if ( $procedure['duration'] == 0 ) {
                                $procedure['duration'] = 1;
                            }
                            
                            $room->procedures()->attach($procedure['id'], ['duration' => $procedure['duration']]);
                        }
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'room', 'id' => $room->id, 'oldValue' => $original, 'newValue' => $room]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.room')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $room->id, 'section' => 'room', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.room')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: rooms. Action: edit');

                return response()->json(['code' => $e->errorInfo[1],'error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $room]);
        }

        $divisions = Division::all();
        return response()->json(['divisions' => $divisions, 'room' => $room]);
    }

    public function delete(Request $request, Room $room)
    {

        try
        {
            if($room->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $room->id, 'section' => 'room']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.room')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $room->id, 'section' => 'room', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.room')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: rooms. Action: delete');
            
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function active(Request $request, Room $room)
    {
        try
        {
            $original = new Room();
            foreach($room->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $room->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'rooms', 'id' => $room->id, 'oldValue' => $original, 'newValue' => $room, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: rooms. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $room]);
    }
}
