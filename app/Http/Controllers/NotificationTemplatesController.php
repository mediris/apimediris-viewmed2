<?php

namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\NotificationTemplate;
    use App\Http\Requests;
    use Activity;
    use Log;

class NotificationTemplatesController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $routeTokens = $request->route()->getCompiled()->getTokens();
            if ( strpos( $routeTokens[0][1], "showactive" ) !== false ) {
                $templates = NotificationTemplate::where('active', true)->orderBy('description', 'asc')->get();
            } else {
                $templates = NotificationTemplate::orderBy('description', 'asc')->get();    
            }

            return $templates;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: notificationtemplates. Action: index');
            
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(NotificationTemplate $template)
    {
        return $template;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            
            $this->validate($request, [
                'description' => 'required|max:25',
                'template'  => 'required'
            ]);

            $template = new NotificationTemplate($request->all());

            try
            {
                if( $template->save()) {
                    Activity::log(trans('tracking.create', ['section' => 'notification_template', 'id' => $template->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-success');
                } else {
                    Activity::log(trans('tracking.attempt', ['section' => 'notification_template', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: notification templates. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $template->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, NotificationTemplate $template)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'template'  => 'required'
            ]);

            $template->active = 0;

            $original = new NotificationTemplate();
            foreach($template->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            try
            {
                if($template->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'notification_template', 'id' => $template->id, 'oldValue' => $original, 'newValue' => $template]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $template->id, 'section' => 'notification_template', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Nottification templates. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $template]);
        }
    }

    
    public function active(Request $request, NotificationTemplate $template) {
        try {
            $original = new NotificationTemplate();
            foreach( $template->getOriginal() as $key => $value ) {
                $original->$key = $value;
            }
            $template->active();

            Activity::log(trans('tracking.edit', ['section' => 'notification_templates', 'id' => $template->id, 'oldValue' => $original, 'newValue' => $template, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        } catch(\Exception $e) {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: templates. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $template]);
    }
}
