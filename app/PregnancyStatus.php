<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PregnancyStatus extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un PregnancyStatus tiene muchos ServiceRequests.
     */
    public function serviceRequests()
    {
        return $this->hasMany(ServiceRequest::class);
    }
}
