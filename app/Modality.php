<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
      'active', 'name', 'level', 'parent_id', 'description', 'requires_additional_fields'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Modality tiene un Modality padre.
     */
    public function parentModality()
    {
        return $this->hasOne(Modality::class, 'id', 'parent_id');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Modality tiene muchos Modalities hijos.
     */
    public function childModalities()
    {
        return $this->hasMany(Modality::class, 'parent_id', 'id');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Modality tiene muchos Equipment.
     */
    public function equipment()
    {
        return $this->hasMany(Equipment::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Modality tiene muchos Steps.
     */
    public function steps()
    {
        return $this->hasMany(Step::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Diego Oliveros
     * @objetivo: Relación: Una Modalidad tiene muchos RequestedProcedures.
     */
    public function requestedProcedures()
    {
        return $this->hasMany(RequestedProcedure::class);
    }

    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
