<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'active', 'appointment_date_start',
        'appointment_date_end', 'appointment_status_id',
        'patient_identification_id', 'patient_first_name',
        'patient_last_name', 'patient_telephone_number',
        'patient_cellphone_number', 'patient_email',
        'observations', 'applicant', 'referring',
        'patient_id', 'user_id', 'room_id', 'procedure_id',
        'modality_id','cancelation_date', 'procedure_duration',
        'blocks_numbers', 'equipment_id',
        'string_start_unique', 'string_end_unique',
        'cancelation_reason', 'cancelation_user_id',
        'cancelation_date',
        'procedure_contrast_study',
        'patient_allergies',
        'patient_weight',
        'patient_height',
        'patient_abdominal_circumference',
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Appointmen tiene un AppointmentStatus.
     */
    public function appointmentStatus()
    {
        return $this->belongsTo(AppointmentStatus::class);
    }


    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Appointmen tiene un Room.
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Appointmen tiene un Procedure.
     */
    public function procedure()
    {
        return $this->belongsTo(Procedure::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo status. 1 => creada(created), 2 => agendada(scheduled), 3 => admitida(admitted), 4 => anulada(discharged)
     */
    public function active($status = null)
    {

        switch ($status) {
            case 1:

                $this->active = 1;
                $this->appointment_status_id = $status;
                
                break;

            case 2:

                $this->active = 1;
                $this->appointment_status_id = $status;

                break;

            case 3:

                $this->active = 1;
                $this->appointment_status_id = $status;
                
                break;

            case 4:

                $this->active = 0;
                $this->appointment_status_id = $status;

                break;
            
            default:
                $this->active = $this->active == 1 ? 0 : 1;
                break;
        }
            
        $this->save();
        
    }

    public static function boot() {
        parent::boot();

        static::updating(function ($appointment) {
            if($appointment->getOriginal('appointment_date_start') != $appointment->appointment_date_start) {
                $appointment->previous_appointment_date_start = $appointment->getOriginal('appointment_date_start');
            }
        });
    }

}
