<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addendums extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
    	'active',
    	'user_id',
    	'requested_procedure_id',
    	'date',
    	'text'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un addendum perteneces a un RequestedProcedure.
     */
    public function requestedProcedure()
    {
        return $this->belongsTo(RequestedProcedure::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa. 
     */
    public function active()
    {

    	$this->active = $this->active == 0 ? 1 : 0;
            
        $this->save();
        
    }
}
