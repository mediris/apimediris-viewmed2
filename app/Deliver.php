<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliver extends Model
{
    protected $fillable = [
        'date', 'receptor_id', 'receptor_name', 'file', 'cd', 'num_cd', 'plates', 'num_plates', 'responsable_id', 'responsable_name', 'requested_procedure_id', 'observations'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Deliver tiene un RequestedProcedure.
     */
    public function requestedProcedure()
    {
        return $this->belongsTo(RequestedProcedure::class);
    }
}
