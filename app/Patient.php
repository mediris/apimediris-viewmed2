<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $guarded = ['id', 'api_token', 'user_id'];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Patient tiene un Sex.
     */
    public function sex()
    {
        return $this->belongsTo(Sex::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Patient tiene un Responsable.
     */
    public function responsable()
    {
        return $this->belongsTo(Responsable::class);
    }
}
