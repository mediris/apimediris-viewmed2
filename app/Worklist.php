<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worklist extends Model
{
	/**
	 * The database name used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysqlwl';
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orden';
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'aetitle', 'procsetpstartdate', 'procsetpstartdatetime', 'modality', 'refp', 'psd', 'psid', 'rpid', 'rpd', 'siuid', 'an', 'rp', 'rpn', 'patn', 'pid', 'pabd', 'ps'
    ];
}
