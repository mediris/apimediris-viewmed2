<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlatesSize extends Model
{

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un PlatesSize tiene muchos RequestedProcedures.
     */
    public function requestedProcedures()
    {
        return $this->hasMany(RequestedProcedure::class);
    }

}
