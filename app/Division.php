<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name', 'active',
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Division tiene muchos Rooms.
     */
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
